package co.edu.usbcali.wimbledon.presentation.businessDelegate;

import co.edu.usbcali.wimbledon.modelo.Arbitro;
import co.edu.usbcali.wimbledon.modelo.Cancha;
import co.edu.usbcali.wimbledon.modelo.Director;
import co.edu.usbcali.wimbledon.modelo.Jugador;
import co.edu.usbcali.wimbledon.modelo.Partido;
import co.edu.usbcali.wimbledon.modelo.PartidoJugador;
import co.edu.usbcali.wimbledon.modelo.Ronda;
import co.edu.usbcali.wimbledon.modelo.Torneo;
import co.edu.usbcali.wimbledon.modelo.control.ArbitroLogic;
import co.edu.usbcali.wimbledon.modelo.control.CanchaLogic;
import co.edu.usbcali.wimbledon.modelo.control.DirectorLogic;
import co.edu.usbcali.wimbledon.modelo.control.IArbitroLogic;
import co.edu.usbcali.wimbledon.modelo.control.ICanchaLogic;
import co.edu.usbcali.wimbledon.modelo.control.IDirectorLogic;
import co.edu.usbcali.wimbledon.modelo.control.IJugadorLogic;
import co.edu.usbcali.wimbledon.modelo.control.IPartidoJugadorLogic;
import co.edu.usbcali.wimbledon.modelo.control.IPartidoLogic;
import co.edu.usbcali.wimbledon.modelo.control.IRondaLogic;
import co.edu.usbcali.wimbledon.modelo.control.ITorneoLogic;
import co.edu.usbcali.wimbledon.modelo.control.JugadorLogic;
import co.edu.usbcali.wimbledon.modelo.control.PartidoJugadorLogic;
import co.edu.usbcali.wimbledon.modelo.control.PartidoLogic;
import co.edu.usbcali.wimbledon.modelo.control.RondaLogic;
import co.edu.usbcali.wimbledon.modelo.control.TorneoLogic;
import co.edu.usbcali.wimbledon.modelo.dto.ArbitroDTO;
import co.edu.usbcali.wimbledon.modelo.dto.CanchaDTO;
import co.edu.usbcali.wimbledon.modelo.dto.DirectorDTO;
import co.edu.usbcali.wimbledon.modelo.dto.JugadorDTO;
import co.edu.usbcali.wimbledon.modelo.dto.PartidoDTO;
import co.edu.usbcali.wimbledon.modelo.dto.PartidoJugadorDTO;
import co.edu.usbcali.wimbledon.modelo.dto.RondaDTO;
import co.edu.usbcali.wimbledon.modelo.dto.TorneoDTO;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IBusinessDelegatorView {
    public List<Arbitro> getArbitro() throws Exception;

    public void saveArbitro(Arbitro entity) throws Exception;

    public void deleteArbitro(Arbitro entity) throws Exception;

    public void updateArbitro(Arbitro entity) throws Exception;

    public Arbitro getArbitro(Long arbId) throws Exception;

    public List<Arbitro> findByCriteriaInArbitro(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Arbitro> findPageArbitro(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberArbitro() throws Exception;

    public List<ArbitroDTO> getDataArbitro() throws Exception;

    public void validateArbitro(Arbitro arbitro) throws Exception;

    public List<Cancha> getCancha() throws Exception;

    public void saveCancha(Cancha entity) throws Exception;

    public void deleteCancha(Cancha entity) throws Exception;

    public void updateCancha(Cancha entity) throws Exception;

    public Cancha getCancha(Long canId) throws Exception;

    public List<Cancha> findByCriteriaInCancha(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Cancha> findPageCancha(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberCancha() throws Exception;

    public List<CanchaDTO> getDataCancha() throws Exception;

    public void validateCancha(Cancha cancha) throws Exception;

    public List<Director> getDirector() throws Exception;

    public void saveDirector(Director entity) throws Exception;

    public void deleteDirector(Director entity) throws Exception;

    public void updateDirector(Director entity) throws Exception;

    public Director getDirector(Long dirId) throws Exception;

    public List<Director> findByCriteriaInDirector(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Director> findPageDirector(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberDirector() throws Exception;

    public List<DirectorDTO> getDataDirector() throws Exception;

    public void validateDirector(Director director) throws Exception;

    public List<Jugador> getJugador() throws Exception;

    public void saveJugador(Jugador entity) throws Exception;

    public void deleteJugador(Jugador entity) throws Exception;

    public void updateJugador(Jugador entity) throws Exception;

    public Jugador getJugador(Long jugId) throws Exception;

    public List<Jugador> findByCriteriaInJugador(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Jugador> findPageJugador(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberJugador() throws Exception;

    public List<JugadorDTO> getDataJugador() throws Exception;

    public void validateJugador(Jugador jugador) throws Exception;

    public List<Partido> getPartido() throws Exception;

    public void savePartido(Partido entity) throws Exception;

    public void deletePartido(Partido entity) throws Exception;

    public void updatePartido(Partido entity) throws Exception;

    public Partido getPartido(Long parId) throws Exception;

    public List<Partido> findByCriteriaInPartido(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Partido> findPagePartido(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberPartido() throws Exception;

    public List<PartidoDTO> getDataPartido() throws Exception;

    public void validatePartido(Partido partido) throws Exception;

    public List<PartidoJugador> getPartidoJugador() throws Exception;

    public void savePartidoJugador(PartidoJugador entity)
        throws Exception;

    public void deletePartidoJugador(PartidoJugador entity)
        throws Exception;

    public void updatePartidoJugador(PartidoJugador entity)
        throws Exception;

    public PartidoJugador getPartidoJugador(Long parjugId)
        throws Exception;

    public List<PartidoJugador> findByCriteriaInPartidoJugador(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception;

    public List<PartidoJugador> findPagePartidoJugador(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberPartidoJugador() throws Exception;

    public List<PartidoJugadorDTO> getDataPartidoJugador()
        throws Exception;

    public void validatePartidoJugador(PartidoJugador partidoJugador)
        throws Exception;

    public List<Ronda> getRonda() throws Exception;

    public void saveRonda(Ronda entity) throws Exception;

    public void deleteRonda(Ronda entity) throws Exception;

    public void updateRonda(Ronda entity) throws Exception;

    public Ronda getRonda(Long ronId) throws Exception;

    public List<Ronda> findByCriteriaInRonda(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Ronda> findPageRonda(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberRonda() throws Exception;

    public List<RondaDTO> getDataRonda() throws Exception;

    public void validateRonda(Ronda ronda) throws Exception;

    public List<Torneo> getTorneo() throws Exception;

    public void saveTorneo(Torneo entity) throws Exception;

    public void deleteTorneo(Torneo entity) throws Exception;

    public void updateTorneo(Torneo entity) throws Exception;

    public Torneo getTorneo(Long torId) throws Exception;

    public List<Torneo> findByCriteriaInTorneo(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Torneo> findPageTorneo(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberTorneo() throws Exception;

    public List<TorneoDTO> getDataTorneo() throws Exception;

    public void validateTorneo(Torneo torneo) throws Exception;
}
