package co.edu.usbcali.wimbledon.presentation.businessDelegate;

import co.edu.usbcali.wimbledon.modelo.Arbitro;
import co.edu.usbcali.wimbledon.modelo.Cancha;
import co.edu.usbcali.wimbledon.modelo.Director;
import co.edu.usbcali.wimbledon.modelo.Jugador;
import co.edu.usbcali.wimbledon.modelo.Partido;
import co.edu.usbcali.wimbledon.modelo.PartidoJugador;
import co.edu.usbcali.wimbledon.modelo.Ronda;
import co.edu.usbcali.wimbledon.modelo.Torneo;
import co.edu.usbcali.wimbledon.modelo.control.ArbitroLogic;
import co.edu.usbcali.wimbledon.modelo.control.CanchaLogic;
import co.edu.usbcali.wimbledon.modelo.control.DirectorLogic;
import co.edu.usbcali.wimbledon.modelo.control.IArbitroLogic;
import co.edu.usbcali.wimbledon.modelo.control.ICanchaLogic;
import co.edu.usbcali.wimbledon.modelo.control.IDirectorLogic;
import co.edu.usbcali.wimbledon.modelo.control.IJugadorLogic;
import co.edu.usbcali.wimbledon.modelo.control.IPartidoJugadorLogic;
import co.edu.usbcali.wimbledon.modelo.control.IPartidoLogic;
import co.edu.usbcali.wimbledon.modelo.control.IRondaLogic;
import co.edu.usbcali.wimbledon.modelo.control.ITorneoLogic;
import co.edu.usbcali.wimbledon.modelo.control.JugadorLogic;
import co.edu.usbcali.wimbledon.modelo.control.PartidoJugadorLogic;
import co.edu.usbcali.wimbledon.modelo.control.PartidoLogic;
import co.edu.usbcali.wimbledon.modelo.control.RondaLogic;
import co.edu.usbcali.wimbledon.modelo.control.TorneoLogic;
import co.edu.usbcali.wimbledon.modelo.dto.ArbitroDTO;
import co.edu.usbcali.wimbledon.modelo.dto.CanchaDTO;
import co.edu.usbcali.wimbledon.modelo.dto.DirectorDTO;
import co.edu.usbcali.wimbledon.modelo.dto.JugadorDTO;
import co.edu.usbcali.wimbledon.modelo.dto.PartidoDTO;
import co.edu.usbcali.wimbledon.modelo.dto.PartidoJugadorDTO;
import co.edu.usbcali.wimbledon.modelo.dto.RondaDTO;
import co.edu.usbcali.wimbledon.modelo.dto.TorneoDTO;
import co.edu.usbcali.wimbledon.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import java.sql.*;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Use a Business Delegate to reduce coupling between presentation-tier clients and business services.
* The Business Delegate hides the underlying implementation details of the business service, such as lookup and access details of the EJB architecture.
*
* The Business Delegate acts as a client-side business abstraction; it provides an abstraction for, and thus hides,
* the implementation of the business services. Using a Business Delegate reduces the coupling between presentation-tier clients and
* the system's business services. Depending on the implementation strategy, the Business Delegate may shield clients from possible
* volatility in the implementation of the business service API. Potentially, this reduces the number of changes that must be made to the
* presentation-tier client code when the business service API or its underlying implementation changes.
*
* However, interface methods in the Business Delegate may still require modification if the underlying business service API changes.
* Admittedly, though, it is more likely that changes will be made to the business service rather than to the Business Delegate.
*
* Often, developers are skeptical when a design goal such as abstracting the business layer causes additional upfront work in return
* for future gains. However, using this pattern or its strategies results in only a small amount of additional upfront work and provides
* considerable benefits. The main benefit is hiding the details of the underlying service. For example, the client can become transparent
* to naming and lookup services. The Business Delegate also handles the exceptions from the business services, such as java.rmi.Remote
* exceptions, Java Messages Service (JMS) exceptions and so on. The Business Delegate may intercept such service level exceptions and
* generate application level exceptions instead. Application level exceptions are easier to handle by the clients, and may be user friendly.
* The Business Delegate may also transparently perform any retry or recovery operations necessary in the event of a service failure without
* exposing the client to the problem until it is determined that the problem is not resolvable. These gains present a compelling reason to
* use the pattern.
*
* Another benefit is that the delegate may cache results and references to remote business services. Caching can significantly improve performance,
* because it limits unnecessary and potentially costly round trips over the network.
*
* A Business Delegate uses a component called the Lookup Service. The Lookup Service is responsible for hiding the underlying implementation
* details of the business service lookup code. The Lookup Service may be written as part of the Delegate, but we recommend that it be
* implemented as a separate component, as outlined in the Service Locator pattern (See "Service Locator" on page 368.)
*
* When the Business Delegate is used with a Session Facade, typically there is a one-to-one relationship between the two.
* This one-to-one relationship exists because logic that might have been encapsulated in a Business Delegate relating to its interaction
* with multiple business services (creating a one-to-many relationship) will often be factored back into a Session Facade.
*
* Finally, it should be noted that this pattern could be used to reduce coupling between other tiers, not simply the presentation and the
* business tiers.
*
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Scope("singleton")
@Service("BusinessDelegatorView")
public class BusinessDelegatorView implements IBusinessDelegatorView {
    private static final Logger log = LoggerFactory.getLogger(BusinessDelegatorView.class);
    @Autowired
    private IArbitroLogic arbitroLogic;
    @Autowired
    private ICanchaLogic canchaLogic;
    @Autowired
    private IDirectorLogic directorLogic;
    @Autowired
    private IJugadorLogic jugadorLogic;
    @Autowired
    private IPartidoLogic partidoLogic;
    @Autowired
    private IPartidoJugadorLogic partidoJugadorLogic;
    @Autowired
    private IRondaLogic rondaLogic;
    @Autowired
    private ITorneoLogic torneoLogic;

    public List<Arbitro> getArbitro() throws Exception {
        return arbitroLogic.getArbitro();
    }

    public void saveArbitro(Arbitro entity) throws Exception {
        arbitroLogic.saveArbitro(entity);
    }

    public void deleteArbitro(Arbitro entity) throws Exception {
        arbitroLogic.deleteArbitro(entity);
    }

    public void updateArbitro(Arbitro entity) throws Exception {
        arbitroLogic.updateArbitro(entity);
    }

    public Arbitro getArbitro(Long arbId) throws Exception {
        Arbitro arbitro = null;

        try {
            arbitro = arbitroLogic.getArbitro(arbId);
        } catch (Exception e) {
            throw e;
        }

        return arbitro;
    }

    public List<Arbitro> findByCriteriaInArbitro(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return arbitroLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Arbitro> findPageArbitro(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return arbitroLogic.findPageArbitro(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberArbitro() throws Exception {
        return arbitroLogic.findTotalNumberArbitro();
    }

    public List<ArbitroDTO> getDataArbitro() throws Exception {
        return arbitroLogic.getDataArbitro();
    }

    public void validateArbitro(Arbitro arbitro) throws Exception {
        arbitroLogic.validateArbitro(arbitro);
    }

    public List<Cancha> getCancha() throws Exception {
        return canchaLogic.getCancha();
    }

    public void saveCancha(Cancha entity) throws Exception {
        canchaLogic.saveCancha(entity);
    }

    public void deleteCancha(Cancha entity) throws Exception {
        canchaLogic.deleteCancha(entity);
    }

    public void updateCancha(Cancha entity) throws Exception {
        canchaLogic.updateCancha(entity);
    }

    public Cancha getCancha(Long canId) throws Exception {
        Cancha cancha = null;

        try {
            cancha = canchaLogic.getCancha(canId);
        } catch (Exception e) {
            throw e;
        }

        return cancha;
    }

    public List<Cancha> findByCriteriaInCancha(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return canchaLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Cancha> findPageCancha(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return canchaLogic.findPageCancha(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberCancha() throws Exception {
        return canchaLogic.findTotalNumberCancha();
    }

    public List<CanchaDTO> getDataCancha() throws Exception {
        return canchaLogic.getDataCancha();
    }

    public void validateCancha(Cancha cancha) throws Exception {
        canchaLogic.validateCancha(cancha);
    }

    public List<Director> getDirector() throws Exception {
        return directorLogic.getDirector();
    }

    public void saveDirector(Director entity) throws Exception {
        directorLogic.saveDirector(entity);
    }

    public void deleteDirector(Director entity) throws Exception {
        directorLogic.deleteDirector(entity);
    }

    public void updateDirector(Director entity) throws Exception {
        directorLogic.updateDirector(entity);
    }

    public Director getDirector(Long dirId) throws Exception {
        Director director = null;

        try {
            director = directorLogic.getDirector(dirId);
        } catch (Exception e) {
            throw e;
        }

        return director;
    }

    public List<Director> findByCriteriaInDirector(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return directorLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Director> findPageDirector(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return directorLogic.findPageDirector(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberDirector() throws Exception {
        return directorLogic.findTotalNumberDirector();
    }

    public List<DirectorDTO> getDataDirector() throws Exception {
        return directorLogic.getDataDirector();
    }

    public void validateDirector(Director director) throws Exception {
        directorLogic.validateDirector(director);
    }

    public List<Jugador> getJugador() throws Exception {
        return jugadorLogic.getJugador();
    }

    public void saveJugador(Jugador entity) throws Exception {
        jugadorLogic.saveJugador(entity);
    }

    public void deleteJugador(Jugador entity) throws Exception {
        jugadorLogic.deleteJugador(entity);
    }

    public void updateJugador(Jugador entity) throws Exception {
        jugadorLogic.updateJugador(entity);
    }

    public Jugador getJugador(Long jugId) throws Exception {
        Jugador jugador = null;

        try {
            jugador = jugadorLogic.getJugador(jugId);
        } catch (Exception e) {
            throw e;
        }

        return jugador;
    }

    public List<Jugador> findByCriteriaInJugador(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return jugadorLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Jugador> findPageJugador(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return jugadorLogic.findPageJugador(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberJugador() throws Exception {
        return jugadorLogic.findTotalNumberJugador();
    }

    public List<JugadorDTO> getDataJugador() throws Exception {
        return jugadorLogic.getDataJugador();
    }

    public void validateJugador(Jugador jugador) throws Exception {
        jugadorLogic.validateJugador(jugador);
    }

    public List<Partido> getPartido() throws Exception {
        return partidoLogic.getPartido();
    }

    public void savePartido(Partido entity) throws Exception {
        partidoLogic.savePartido(entity);
    }

    public void deletePartido(Partido entity) throws Exception {
        partidoLogic.deletePartido(entity);
    }

    public void updatePartido(Partido entity) throws Exception {
        partidoLogic.updatePartido(entity);
    }

    public Partido getPartido(Long parId) throws Exception {
        Partido partido = null;

        try {
            partido = partidoLogic.getPartido(parId);
        } catch (Exception e) {
            throw e;
        }

        return partido;
    }

    public List<Partido> findByCriteriaInPartido(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return partidoLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Partido> findPagePartido(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return partidoLogic.findPagePartido(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberPartido() throws Exception {
        return partidoLogic.findTotalNumberPartido();
    }

    public List<PartidoDTO> getDataPartido() throws Exception {
        return partidoLogic.getDataPartido();
    }

    public void validatePartido(Partido partido) throws Exception {
        partidoLogic.validatePartido(partido);
    }

    public List<PartidoJugador> getPartidoJugador() throws Exception {
        return partidoJugadorLogic.getPartidoJugador();
    }

    public void savePartidoJugador(PartidoJugador entity)
        throws Exception {
        partidoJugadorLogic.savePartidoJugador(entity);
    }

    public void deletePartidoJugador(PartidoJugador entity)
        throws Exception {
        partidoJugadorLogic.deletePartidoJugador(entity);
    }

    public void updatePartidoJugador(PartidoJugador entity)
        throws Exception {
        partidoJugadorLogic.updatePartidoJugador(entity);
    }

    public PartidoJugador getPartidoJugador(Long parjugId)
        throws Exception {
        PartidoJugador partidoJugador = null;

        try {
            partidoJugador = partidoJugadorLogic.getPartidoJugador(parjugId);
        } catch (Exception e) {
            throw e;
        }

        return partidoJugador;
    }

    public List<PartidoJugador> findByCriteriaInPartidoJugador(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception {
        return partidoJugadorLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<PartidoJugador> findPagePartidoJugador(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return partidoJugadorLogic.findPagePartidoJugador(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberPartidoJugador() throws Exception {
        return partidoJugadorLogic.findTotalNumberPartidoJugador();
    }

    public List<PartidoJugadorDTO> getDataPartidoJugador()
        throws Exception {
        return partidoJugadorLogic.getDataPartidoJugador();
    }

    public void validatePartidoJugador(PartidoJugador partidoJugador)
        throws Exception {
        partidoJugadorLogic.validatePartidoJugador(partidoJugador);
    }

    public List<Ronda> getRonda() throws Exception {
        return rondaLogic.getRonda();
    }

    public void saveRonda(Ronda entity) throws Exception {
        rondaLogic.saveRonda(entity);
    }

    public void deleteRonda(Ronda entity) throws Exception {
        rondaLogic.deleteRonda(entity);
    }

    public void updateRonda(Ronda entity) throws Exception {
        rondaLogic.updateRonda(entity);
    }

    public Ronda getRonda(Long ronId) throws Exception {
        Ronda ronda = null;

        try {
            ronda = rondaLogic.getRonda(ronId);
        } catch (Exception e) {
            throw e;
        }

        return ronda;
    }

    public List<Ronda> findByCriteriaInRonda(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return rondaLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Ronda> findPageRonda(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return rondaLogic.findPageRonda(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberRonda() throws Exception {
        return rondaLogic.findTotalNumberRonda();
    }

    public List<RondaDTO> getDataRonda() throws Exception {
        return rondaLogic.getDataRonda();
    }

    public void validateRonda(Ronda ronda) throws Exception {
        rondaLogic.validateRonda(ronda);
    }

    public List<Torneo> getTorneo() throws Exception {
        return torneoLogic.getTorneo();
    }

    public void saveTorneo(Torneo entity) throws Exception {
        torneoLogic.saveTorneo(entity);
    }

    public void deleteTorneo(Torneo entity) throws Exception {
        torneoLogic.deleteTorneo(entity);
    }

    public void updateTorneo(Torneo entity) throws Exception {
        torneoLogic.updateTorneo(entity);
    }

    public Torneo getTorneo(Long torId) throws Exception {
        Torneo torneo = null;

        try {
            torneo = torneoLogic.getTorneo(torId);
        } catch (Exception e) {
            throw e;
        }

        return torneo;
    }

    public List<Torneo> findByCriteriaInTorneo(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return torneoLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Torneo> findPageTorneo(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return torneoLogic.findPageTorneo(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberTorneo() throws Exception {
        return torneoLogic.findTotalNumberTorneo();
    }

    public List<TorneoDTO> getDataTorneo() throws Exception {
        return torneoLogic.getDataTorneo();
    }

    public void validateTorneo(Torneo torneo) throws Exception {
        torneoLogic.validateTorneo(torneo);
    }
}
