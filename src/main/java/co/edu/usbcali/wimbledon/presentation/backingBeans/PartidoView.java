package co.edu.usbcali.wimbledon.presentation.backingBeans;

import co.edu.usbcali.wimbledon.exceptions.*;
import co.edu.usbcali.wimbledon.modelo.*;
import co.edu.usbcali.wimbledon.modelo.dto.PartidoDTO;
import co.edu.usbcali.wimbledon.presentation.businessDelegate.*;
import co.edu.usbcali.wimbledon.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class PartidoView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(PartidoView.class);
    private InputText txtDescripcion;
    private InputText txtNivel;
    private InputText txtTiempo;
    private InputText txtArbId_Arbitro;
    private InputText txtCanId_Cancha;
    private InputText txtRonId_Ronda;
    private InputText txtParId;
    private Calendar txtFecha;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<PartidoDTO> data;
    private PartidoDTO selectedPartido;
    private Partido entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public PartidoView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedPartido = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedPartido = null;

        if (txtDescripcion != null) {
            txtDescripcion.setValue(null);
            txtDescripcion.setDisabled(true);
        }

        if (txtNivel != null) {
            txtNivel.setValue(null);
            txtNivel.setDisabled(true);
        }

        if (txtTiempo != null) {
            txtTiempo.setValue(null);
            txtTiempo.setDisabled(true);
        }

        if (txtArbId_Arbitro != null) {
            txtArbId_Arbitro.setValue(null);
            txtArbId_Arbitro.setDisabled(true);
        }

        if (txtCanId_Cancha != null) {
            txtCanId_Cancha.setValue(null);
            txtCanId_Cancha.setDisabled(true);
        }

        if (txtRonId_Ronda != null) {
            txtRonId_Ronda.setValue(null);
            txtRonId_Ronda.setDisabled(true);
        }

        if (txtFecha != null) {
            txtFecha.setValue(null);
            txtFecha.setDisabled(true);
        }

        if (txtParId != null) {
            txtParId.setValue(null);
            txtParId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFecha() {
        Date inputDate = (Date) txtFecha.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            Long parId = FacesUtils.checkLong(txtParId);
            entity = (parId != null) ? businessDelegatorView.getPartido(parId)
                                     : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtDescripcion.setDisabled(false);
            txtNivel.setDisabled(false);
            txtTiempo.setDisabled(false);
            txtArbId_Arbitro.setDisabled(false);
            txtCanId_Cancha.setDisabled(false);
            txtRonId_Ronda.setDisabled(false);
            txtFecha.setDisabled(false);
            txtParId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtDescripcion.setValue(entity.getDescripcion());
            txtDescripcion.setDisabled(false);
            txtFecha.setValue(entity.getFecha());
            txtFecha.setDisabled(false);
            txtNivel.setValue(entity.getNivel());
            txtNivel.setDisabled(false);
            txtTiempo.setValue(entity.getTiempo());
            txtTiempo.setDisabled(false);
            txtArbId_Arbitro.setValue(entity.getArbitro().getArbId());
            txtArbId_Arbitro.setDisabled(false);
            txtCanId_Cancha.setValue(entity.getCancha().getCanId());
            txtCanId_Cancha.setDisabled(false);
            txtRonId_Ronda.setValue(entity.getRonda().getRonId());
            txtRonId_Ronda.setDisabled(false);
            txtParId.setValue(entity.getParId());
            txtParId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedPartido = (PartidoDTO) (evt.getComponent().getAttributes()
                                           .get("selectedPartido"));
        txtDescripcion.setValue(selectedPartido.getDescripcion());
        txtDescripcion.setDisabled(false);
        txtFecha.setValue(selectedPartido.getFecha());
        txtFecha.setDisabled(false);
        txtNivel.setValue(selectedPartido.getNivel());
        txtNivel.setDisabled(false);
        txtTiempo.setValue(selectedPartido.getTiempo());
        txtTiempo.setDisabled(false);
        txtArbId_Arbitro.setValue(selectedPartido.getArbId_Arbitro());
        txtArbId_Arbitro.setDisabled(false);
        txtCanId_Cancha.setValue(selectedPartido.getCanId_Cancha());
        txtCanId_Cancha.setDisabled(false);
        txtRonId_Ronda.setValue(selectedPartido.getRonId_Ronda());
        txtRonId_Ronda.setDisabled(false);
        txtParId.setValue(selectedPartido.getParId());
        txtParId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedPartido == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Partido();

            Long parId = FacesUtils.checkLong(txtParId);

            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setFecha(FacesUtils.checkDate(txtFecha));
            entity.setNivel(FacesUtils.checkLong(txtNivel));
            entity.setParId(parId);
            entity.setTiempo(FacesUtils.checkLong(txtTiempo));
            entity.setArbitro((FacesUtils.checkLong(txtArbId_Arbitro) != null)
                ? businessDelegatorView.getArbitro(FacesUtils.checkLong(
                        txtArbId_Arbitro)) : null);
            entity.setCancha((FacesUtils.checkLong(txtCanId_Cancha) != null)
                ? businessDelegatorView.getCancha(FacesUtils.checkLong(
                        txtCanId_Cancha)) : null);
            entity.setRonda((FacesUtils.checkLong(txtRonId_Ronda) != null)
                ? businessDelegatorView.getRonda(FacesUtils.checkLong(
                        txtRonId_Ronda)) : null);
            businessDelegatorView.savePartido(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long parId = new Long(selectedPartido.getParId());
                entity = businessDelegatorView.getPartido(parId);
            }

            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setFecha(FacesUtils.checkDate(txtFecha));
            entity.setNivel(FacesUtils.checkLong(txtNivel));
            entity.setTiempo(FacesUtils.checkLong(txtTiempo));
            entity.setArbitro((FacesUtils.checkLong(txtArbId_Arbitro) != null)
                ? businessDelegatorView.getArbitro(FacesUtils.checkLong(
                        txtArbId_Arbitro)) : null);
            entity.setCancha((FacesUtils.checkLong(txtCanId_Cancha) != null)
                ? businessDelegatorView.getCancha(FacesUtils.checkLong(
                        txtCanId_Cancha)) : null);
            entity.setRonda((FacesUtils.checkLong(txtRonId_Ronda) != null)
                ? businessDelegatorView.getRonda(FacesUtils.checkLong(
                        txtRonId_Ronda)) : null);
            businessDelegatorView.updatePartido(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedPartido = (PartidoDTO) (evt.getComponent().getAttributes()
                                               .get("selectedPartido"));

            Long parId = new Long(selectedPartido.getParId());
            entity = businessDelegatorView.getPartido(parId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long parId = FacesUtils.checkLong(txtParId);
            entity = businessDelegatorView.getPartido(parId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deletePartido(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String descripcion, Date fecha,
        Long nivel, Long parId, Long tiempo, Long arbId_Arbitro,
        Long canId_Cancha, Long ronId_Ronda) throws Exception {
        try {
            entity.setDescripcion(FacesUtils.checkString(descripcion));
            entity.setFecha(FacesUtils.checkDate(fecha));
            entity.setNivel(FacesUtils.checkLong(nivel));
            entity.setTiempo(FacesUtils.checkLong(tiempo));
            businessDelegatorView.updatePartido(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("PartidoView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtDescripcion() {
        return txtDescripcion;
    }

    public void setTxtDescripcion(InputText txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    public InputText getTxtNivel() {
        return txtNivel;
    }

    public void setTxtNivel(InputText txtNivel) {
        this.txtNivel = txtNivel;
    }

    public InputText getTxtTiempo() {
        return txtTiempo;
    }

    public void setTxtTiempo(InputText txtTiempo) {
        this.txtTiempo = txtTiempo;
    }

    public InputText getTxtArbId_Arbitro() {
        return txtArbId_Arbitro;
    }

    public void setTxtArbId_Arbitro(InputText txtArbId_Arbitro) {
        this.txtArbId_Arbitro = txtArbId_Arbitro;
    }

    public InputText getTxtCanId_Cancha() {
        return txtCanId_Cancha;
    }

    public void setTxtCanId_Cancha(InputText txtCanId_Cancha) {
        this.txtCanId_Cancha = txtCanId_Cancha;
    }

    public InputText getTxtRonId_Ronda() {
        return txtRonId_Ronda;
    }

    public void setTxtRonId_Ronda(InputText txtRonId_Ronda) {
        this.txtRonId_Ronda = txtRonId_Ronda;
    }

    public Calendar getTxtFecha() {
        return txtFecha;
    }

    public void setTxtFecha(Calendar txtFecha) {
        this.txtFecha = txtFecha;
    }

    public InputText getTxtParId() {
        return txtParId;
    }

    public void setTxtParId(InputText txtParId) {
        this.txtParId = txtParId;
    }

    public List<PartidoDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataPartido();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<PartidoDTO> partidoDTO) {
        this.data = partidoDTO;
    }

    public PartidoDTO getSelectedPartido() {
        return selectedPartido;
    }

    public void setSelectedPartido(PartidoDTO partido) {
        this.selectedPartido = partido;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}
