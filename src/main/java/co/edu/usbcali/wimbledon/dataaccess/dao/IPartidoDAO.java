package co.edu.usbcali.wimbledon.dataaccess.dao;

import co.edu.usbcali.wimbledon.dataaccess.api.Dao;
import co.edu.usbcali.wimbledon.modelo.Partido;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Interface for   PartidoDAO.
*
*/
public interface IPartidoDAO extends Dao<Partido, Long> {
}
