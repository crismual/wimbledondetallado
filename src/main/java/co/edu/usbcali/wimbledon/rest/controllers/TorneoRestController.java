package co.edu.usbcali.wimbledon.rest.controllers;

import co.edu.usbcali.wimbledon.dto.mapper.ITorneoMapper;
import co.edu.usbcali.wimbledon.modelo.*;
import co.edu.usbcali.wimbledon.modelo.dto.TorneoDTO;
import co.edu.usbcali.wimbledon.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/torneo")
public class TorneoRestController {
    private static final Logger log = LoggerFactory.getLogger(TorneoRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private ITorneoMapper torneoMapper;

    @PostMapping(value = "/saveTorneo")
    public void saveTorneo(@RequestBody
    TorneoDTO torneoDTO) throws Exception {
        try {
            Torneo torneo = torneoMapper.torneoDTOToTorneo(torneoDTO);

            businessDelegatorView.saveTorneo(torneo);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteTorneo/{torId}")
    public void deleteTorneo(@PathVariable("torId")
    Long torId) throws Exception {
        try {
            Torneo torneo = businessDelegatorView.getTorneo(torId);

            businessDelegatorView.deleteTorneo(torneo);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateTorneo/")
    public void updateTorneo(@RequestBody
    TorneoDTO torneoDTO) throws Exception {
        try {
            Torneo torneo = torneoMapper.torneoDTOToTorneo(torneoDTO);

            businessDelegatorView.updateTorneo(torneo);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataTorneo")
    public List<TorneoDTO> getDataTorneo() throws Exception {
        try {
            return businessDelegatorView.getDataTorneo();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getTorneo/{torId}")
    public TorneoDTO getTorneo(@PathVariable("torId")
    Long torId) throws Exception {
        try {
            Torneo torneo = businessDelegatorView.getTorneo(torId);

            return torneoMapper.torneoToTorneoDTO(torneo);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
