package co.edu.usbcali.wimbledon.dataaccess.dao;

import co.edu.usbcali.wimbledon.dataaccess.api.Dao;
import co.edu.usbcali.wimbledon.modelo.Director;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Interface for   DirectorDAO.
*
*/
public interface IDirectorDAO extends Dao<Director, Long> {
}
