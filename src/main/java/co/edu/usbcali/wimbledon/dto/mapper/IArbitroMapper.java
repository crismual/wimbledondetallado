package co.edu.usbcali.wimbledon.dto.mapper;

import co.edu.usbcali.wimbledon.modelo.Arbitro;
import co.edu.usbcali.wimbledon.modelo.dto.ArbitroDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IArbitroMapper {
    public ArbitroDTO arbitroToArbitroDTO(Arbitro arbitro)
        throws Exception;

    public Arbitro arbitroDTOToArbitro(ArbitroDTO arbitroDTO)
        throws Exception;

    public List<ArbitroDTO> listArbitroToListArbitroDTO(List<Arbitro> arbitros)
        throws Exception;

    public List<Arbitro> listArbitroDTOToListArbitro(
        List<ArbitroDTO> arbitroDTOs) throws Exception;
}
