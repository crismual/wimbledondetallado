package co.edu.usbcali.wimbledon.rest.controllers;

import co.edu.usbcali.wimbledon.dto.mapper.IArbitroMapper;
import co.edu.usbcali.wimbledon.modelo.*;
import co.edu.usbcali.wimbledon.modelo.dto.ArbitroDTO;
import co.edu.usbcali.wimbledon.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/arbitro")
public class ArbitroRestController {
    private static final Logger log = LoggerFactory.getLogger(ArbitroRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IArbitroMapper arbitroMapper;

    @PostMapping(value = "/saveArbitro")
    public void saveArbitro(@RequestBody
    ArbitroDTO arbitroDTO) throws Exception {
        try {
            Arbitro arbitro = arbitroMapper.arbitroDTOToArbitro(arbitroDTO);

            businessDelegatorView.saveArbitro(arbitro);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteArbitro/{arbId}")
    public void deleteArbitro(@PathVariable("arbId")
    Long arbId) throws Exception {
        try {
            Arbitro arbitro = businessDelegatorView.getArbitro(arbId);

            businessDelegatorView.deleteArbitro(arbitro);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateArbitro/")
    public void updateArbitro(@RequestBody
    ArbitroDTO arbitroDTO) throws Exception {
        try {
            Arbitro arbitro = arbitroMapper.arbitroDTOToArbitro(arbitroDTO);

            businessDelegatorView.updateArbitro(arbitro);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataArbitro")
    public List<ArbitroDTO> getDataArbitro() throws Exception {
        try {
            return businessDelegatorView.getDataArbitro();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getArbitro/{arbId}")
    public ArbitroDTO getArbitro(@PathVariable("arbId")
    Long arbId) throws Exception {
        try {
            Arbitro arbitro = businessDelegatorView.getArbitro(arbId);

            return arbitroMapper.arbitroToArbitroDTO(arbitro);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
