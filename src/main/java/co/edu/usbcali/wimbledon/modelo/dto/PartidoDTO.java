package co.edu.usbcali.wimbledon.modelo.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.util.Date;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public class PartidoDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(PartidoDTO.class);
    private String descripcion;
    private Date fecha;
    private Long nivel;
    private Long parId;
    private Long tiempo;
    private Long arbId_Arbitro;
    private Long canId_Cancha;
    private Long ronId_Ronda;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getNivel() {
        return nivel;
    }

    public void setNivel(Long nivel) {
        this.nivel = nivel;
    }

    public Long getParId() {
        return parId;
    }

    public void setParId(Long parId) {
        this.parId = parId;
    }

    public Long getTiempo() {
        return tiempo;
    }

    public void setTiempo(Long tiempo) {
        this.tiempo = tiempo;
    }

    public Long getArbId_Arbitro() {
        return arbId_Arbitro;
    }

    public void setArbId_Arbitro(Long arbId_Arbitro) {
        this.arbId_Arbitro = arbId_Arbitro;
    }

    public Long getCanId_Cancha() {
        return canId_Cancha;
    }

    public void setCanId_Cancha(Long canId_Cancha) {
        this.canId_Cancha = canId_Cancha;
    }

    public Long getRonId_Ronda() {
        return ronId_Ronda;
    }

    public void setRonId_Ronda(Long ronId_Ronda) {
        this.ronId_Ronda = ronId_Ronda;
    }
}
