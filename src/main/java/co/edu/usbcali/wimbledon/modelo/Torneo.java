package co.edu.usbcali.wimbledon.modelo;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "torneo", schema = "public")
public class Torneo implements java.io.Serializable {
    @NotNull
    private Long torId;
    private String descripcion;
    private String nombre;
    private Set<Ronda> rondas = new HashSet<Ronda>(0);

    public Torneo() {
    }

    public Torneo(Long torId, String descripcion, String nombre,
        Set<Ronda> rondas) {
        this.torId = torId;
        this.descripcion = descripcion;
        this.nombre = nombre;
        this.rondas = rondas;
    }

    @Id
    @Column(name = "tor_id", unique = true, nullable = false)
    public Long getTorId() {
        return this.torId;
    }

    public void setTorId(Long torId) {
        this.torId = torId;
    }

    @Column(name = "descripcion")
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "nombre")
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "torneo")
    public Set<Ronda> getRondas() {
        return this.rondas;
    }

    public void setRondas(Set<Ronda> rondas) {
        this.rondas = rondas;
    }
}
