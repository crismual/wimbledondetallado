package co.edu.usbcali.wimbledon.dto.mapper;

import co.edu.usbcali.wimbledon.modelo.*;
import co.edu.usbcali.wimbledon.modelo.Partido;
import co.edu.usbcali.wimbledon.modelo.control.*;
import co.edu.usbcali.wimbledon.modelo.dto.PartidoDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class PartidoMapper implements IPartidoMapper {
    private static final Logger log = LoggerFactory.getLogger(PartidoMapper.class);

    /**
    * Logic injected by Spring that manages Arbitro entities
    *
    */
    @Autowired
    IArbitroLogic logicArbitro1;

    /**
    * Logic injected by Spring that manages Cancha entities
    *
    */
    @Autowired
    ICanchaLogic logicCancha2;

    /**
    * Logic injected by Spring that manages Ronda entities
    *
    */
    @Autowired
    IRondaLogic logicRonda3;

    @Transactional(readOnly = true)
    public PartidoDTO partidoToPartidoDTO(Partido partido)
        throws Exception {
        try {
            PartidoDTO partidoDTO = new PartidoDTO();

            partidoDTO.setParId(partido.getParId());
            partidoDTO.setDescripcion((partido.getDescripcion() != null)
                ? partido.getDescripcion() : null);
            partidoDTO.setFecha(partido.getFecha());
            partidoDTO.setNivel((partido.getNivel() != null)
                ? partido.getNivel() : null);
            partidoDTO.setTiempo((partido.getTiempo() != null)
                ? partido.getTiempo() : null);
            partidoDTO.setArbId_Arbitro((partido.getArbitro().getArbId() != null)
                ? partido.getArbitro().getArbId() : null);
            partidoDTO.setCanId_Cancha((partido.getCancha().getCanId() != null)
                ? partido.getCancha().getCanId() : null);
            partidoDTO.setRonId_Ronda((partido.getRonda().getRonId() != null)
                ? partido.getRonda().getRonId() : null);

            return partidoDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Partido partidoDTOToPartido(PartidoDTO partidoDTO)
        throws Exception {
        try {
            Partido partido = new Partido();

            partido.setParId(partidoDTO.getParId());
            partido.setDescripcion((partidoDTO.getDescripcion() != null)
                ? partidoDTO.getDescripcion() : null);
            partido.setFecha(partidoDTO.getFecha());
            partido.setNivel((partidoDTO.getNivel() != null)
                ? partidoDTO.getNivel() : null);
            partido.setTiempo((partidoDTO.getTiempo() != null)
                ? partidoDTO.getTiempo() : null);

            Arbitro arbitro = new Arbitro();

            if (partidoDTO.getArbId_Arbitro() != null) {
                arbitro = logicArbitro1.getArbitro(partidoDTO.getArbId_Arbitro());
            }

            if (arbitro != null) {
                partido.setArbitro(arbitro);
            }

            Cancha cancha = new Cancha();

            if (partidoDTO.getCanId_Cancha() != null) {
                cancha = logicCancha2.getCancha(partidoDTO.getCanId_Cancha());
            }

            if (cancha != null) {
                partido.setCancha(cancha);
            }

            Ronda ronda = new Ronda();

            if (partidoDTO.getRonId_Ronda() != null) {
                ronda = logicRonda3.getRonda(partidoDTO.getRonId_Ronda());
            }

            if (ronda != null) {
                partido.setRonda(ronda);
            }

            return partido;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<PartidoDTO> listPartidoToListPartidoDTO(
        List<Partido> listPartido) throws Exception {
        try {
            List<PartidoDTO> partidoDTOs = new ArrayList<PartidoDTO>();

            for (Partido partido : listPartido) {
                PartidoDTO partidoDTO = partidoToPartidoDTO(partido);

                partidoDTOs.add(partidoDTO);
            }

            return partidoDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Partido> listPartidoDTOToListPartido(
        List<PartidoDTO> listPartidoDTO) throws Exception {
        try {
            List<Partido> listPartido = new ArrayList<Partido>();

            for (PartidoDTO partidoDTO : listPartidoDTO) {
                Partido partido = partidoDTOToPartido(partidoDTO);

                listPartido.add(partido);
            }

            return listPartido;
        } catch (Exception e) {
            throw e;
        }
    }
}
