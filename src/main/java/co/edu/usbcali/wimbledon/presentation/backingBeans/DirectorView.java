package co.edu.usbcali.wimbledon.presentation.backingBeans;

import co.edu.usbcali.wimbledon.exceptions.*;
import co.edu.usbcali.wimbledon.modelo.*;
import co.edu.usbcali.wimbledon.modelo.dto.DirectorDTO;
import co.edu.usbcali.wimbledon.presentation.businessDelegate.*;
import co.edu.usbcali.wimbledon.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class DirectorView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(DirectorView.class);
    private InputText txtApellido;
    private InputText txtCedula;
    private InputText txtCorreo;
    private InputText txtNombre;
    private InputText txtDirId;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<DirectorDTO> data;
    private DirectorDTO selectedDirector;
    private Director entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public DirectorView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedDirector = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedDirector = null;

        if (txtApellido != null) {
            txtApellido.setValue(null);
            txtApellido.setDisabled(true);
        }

        if (txtCedula != null) {
            txtCedula.setValue(null);
            txtCedula.setDisabled(true);
        }

        if (txtCorreo != null) {
            txtCorreo.setValue(null);
            txtCorreo.setDisabled(true);
        }

        if (txtNombre != null) {
            txtNombre.setValue(null);
            txtNombre.setDisabled(true);
        }

        if (txtDirId != null) {
            txtDirId.setValue(null);
            txtDirId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtId() {
        try {
            Long dirId = FacesUtils.checkLong(txtDirId);
            entity = (dirId != null) ? businessDelegatorView.getDirector(dirId)
                                     : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtApellido.setDisabled(false);
            txtCedula.setDisabled(false);
            txtCorreo.setDisabled(false);
            txtNombre.setDisabled(false);
            txtDirId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtApellido.setValue(entity.getApellido());
            txtApellido.setDisabled(false);
            txtCedula.setValue(entity.getCedula());
            txtCedula.setDisabled(false);
            txtCorreo.setValue(entity.getCorreo());
            txtCorreo.setDisabled(false);
            txtNombre.setValue(entity.getNombre());
            txtNombre.setDisabled(false);
            txtDirId.setValue(entity.getDirId());
            txtDirId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedDirector = (DirectorDTO) (evt.getComponent().getAttributes()
                                             .get("selectedDirector"));
        txtApellido.setValue(selectedDirector.getApellido());
        txtApellido.setDisabled(false);
        txtCedula.setValue(selectedDirector.getCedula());
        txtCedula.setDisabled(false);
        txtCorreo.setValue(selectedDirector.getCorreo());
        txtCorreo.setDisabled(false);
        txtNombre.setValue(selectedDirector.getNombre());
        txtNombre.setDisabled(false);
        txtDirId.setValue(selectedDirector.getDirId());
        txtDirId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedDirector == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Director();

            Long dirId = FacesUtils.checkLong(txtDirId);

            entity.setApellido(FacesUtils.checkString(txtApellido));
            entity.setCedula(FacesUtils.checkLong(txtCedula));
            entity.setCorreo(FacesUtils.checkString(txtCorreo));
            entity.setDirId(dirId);
            entity.setNombre(FacesUtils.checkString(txtNombre));
            businessDelegatorView.saveDirector(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long dirId = new Long(selectedDirector.getDirId());
                entity = businessDelegatorView.getDirector(dirId);
            }

            entity.setApellido(FacesUtils.checkString(txtApellido));
            entity.setCedula(FacesUtils.checkLong(txtCedula));
            entity.setCorreo(FacesUtils.checkString(txtCorreo));
            entity.setNombre(FacesUtils.checkString(txtNombre));
            businessDelegatorView.updateDirector(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedDirector = (DirectorDTO) (evt.getComponent().getAttributes()
                                                 .get("selectedDirector"));

            Long dirId = new Long(selectedDirector.getDirId());
            entity = businessDelegatorView.getDirector(dirId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long dirId = FacesUtils.checkLong(txtDirId);
            entity = businessDelegatorView.getDirector(dirId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteDirector(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String apellido, Long cedula,
        String correo, Long dirId, String nombre) throws Exception {
        try {
            entity.setApellido(FacesUtils.checkString(apellido));
            entity.setCedula(FacesUtils.checkLong(cedula));
            entity.setCorreo(FacesUtils.checkString(correo));
            entity.setNombre(FacesUtils.checkString(nombre));
            businessDelegatorView.updateDirector(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("DirectorView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtApellido() {
        return txtApellido;
    }

    public void setTxtApellido(InputText txtApellido) {
        this.txtApellido = txtApellido;
    }

    public InputText getTxtCedula() {
        return txtCedula;
    }

    public void setTxtCedula(InputText txtCedula) {
        this.txtCedula = txtCedula;
    }

    public InputText getTxtCorreo() {
        return txtCorreo;
    }

    public void setTxtCorreo(InputText txtCorreo) {
        this.txtCorreo = txtCorreo;
    }

    public InputText getTxtNombre() {
        return txtNombre;
    }

    public void setTxtNombre(InputText txtNombre) {
        this.txtNombre = txtNombre;
    }

    public InputText getTxtDirId() {
        return txtDirId;
    }

    public void setTxtDirId(InputText txtDirId) {
        this.txtDirId = txtDirId;
    }

    public List<DirectorDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataDirector();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<DirectorDTO> directorDTO) {
        this.data = directorDTO;
    }

    public DirectorDTO getSelectedDirector() {
        return selectedDirector;
    }

    public void setSelectedDirector(DirectorDTO director) {
        this.selectedDirector = director;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}
