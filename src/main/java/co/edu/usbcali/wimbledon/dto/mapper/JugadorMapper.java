package co.edu.usbcali.wimbledon.dto.mapper;

import co.edu.usbcali.wimbledon.modelo.*;
import co.edu.usbcali.wimbledon.modelo.Jugador;
import co.edu.usbcali.wimbledon.modelo.control.*;
import co.edu.usbcali.wimbledon.modelo.dto.JugadorDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class JugadorMapper implements IJugadorMapper {
    private static final Logger log = LoggerFactory.getLogger(JugadorMapper.class);

    @Transactional(readOnly = true)
    public JugadorDTO jugadorToJugadorDTO(Jugador jugador)
        throws Exception {
        try {
            JugadorDTO jugadorDTO = new JugadorDTO();

            jugadorDTO.setJugId(jugador.getJugId());
            jugadorDTO.setApellido((jugador.getApellido() != null)
                ? jugador.getApellido() : null);
            jugadorDTO.setCedula((jugador.getCedula() != null)
                ? jugador.getCedula() : null);
            jugadorDTO.setCorreo((jugador.getCorreo() != null)
                ? jugador.getCorreo() : null);
            jugadorDTO.setEstatura((jugador.getEstatura() != null)
                ? jugador.getEstatura() : null);
            jugadorDTO.setFechaNacimiento(jugador.getFechaNacimiento());
            jugadorDTO.setNombre((jugador.getNombre() != null)
                ? jugador.getNombre() : null);
            jugadorDTO.setPeso((jugador.getPeso() != null) ? jugador.getPeso()
                                                           : null);
            jugadorDTO.setRanking((jugador.getRanking() != null)
                ? jugador.getRanking() : null);

            return jugadorDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Jugador jugadorDTOToJugador(JugadorDTO jugadorDTO)
        throws Exception {
        try {
            Jugador jugador = new Jugador();

            jugador.setJugId(jugadorDTO.getJugId());
            jugador.setApellido((jugadorDTO.getApellido() != null)
                ? jugadorDTO.getApellido() : null);
            jugador.setCedula((jugadorDTO.getCedula() != null)
                ? jugadorDTO.getCedula() : null);
            jugador.setCorreo((jugadorDTO.getCorreo() != null)
                ? jugadorDTO.getCorreo() : null);
            jugador.setEstatura((jugadorDTO.getEstatura() != null)
                ? jugadorDTO.getEstatura() : null);
            jugador.setFechaNacimiento(jugadorDTO.getFechaNacimiento());
            jugador.setNombre((jugadorDTO.getNombre() != null)
                ? jugadorDTO.getNombre() : null);
            jugador.setPeso((jugadorDTO.getPeso() != null)
                ? jugadorDTO.getPeso() : null);
            jugador.setRanking((jugadorDTO.getRanking() != null)
                ? jugadorDTO.getRanking() : null);

            return jugador;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<JugadorDTO> listJugadorToListJugadorDTO(
        List<Jugador> listJugador) throws Exception {
        try {
            List<JugadorDTO> jugadorDTOs = new ArrayList<JugadorDTO>();

            for (Jugador jugador : listJugador) {
                JugadorDTO jugadorDTO = jugadorToJugadorDTO(jugador);

                jugadorDTOs.add(jugadorDTO);
            }

            return jugadorDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Jugador> listJugadorDTOToListJugador(
        List<JugadorDTO> listJugadorDTO) throws Exception {
        try {
            List<Jugador> listJugador = new ArrayList<Jugador>();

            for (JugadorDTO jugadorDTO : listJugadorDTO) {
                Jugador jugador = jugadorDTOToJugador(jugadorDTO);

                listJugador.add(jugador);
            }

            return listJugador;
        } catch (Exception e) {
            throw e;
        }
    }
}
