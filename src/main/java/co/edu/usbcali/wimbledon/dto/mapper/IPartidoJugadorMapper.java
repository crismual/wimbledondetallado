package co.edu.usbcali.wimbledon.dto.mapper;

import co.edu.usbcali.wimbledon.modelo.PartidoJugador;
import co.edu.usbcali.wimbledon.modelo.dto.PartidoJugadorDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IPartidoJugadorMapper {
    public PartidoJugadorDTO partidoJugadorToPartidoJugadorDTO(
        PartidoJugador partidoJugador) throws Exception;

    public PartidoJugador partidoJugadorDTOToPartidoJugador(
        PartidoJugadorDTO partidoJugadorDTO) throws Exception;

    public List<PartidoJugadorDTO> listPartidoJugadorToListPartidoJugadorDTO(
        List<PartidoJugador> partidoJugadors) throws Exception;

    public List<PartidoJugador> listPartidoJugadorDTOToListPartidoJugador(
        List<PartidoJugadorDTO> partidoJugadorDTOs) throws Exception;
}
