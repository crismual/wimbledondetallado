package co.edu.usbcali.wimbledon.modelo;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "partido", schema = "public")
public class Partido implements java.io.Serializable {
    @NotNull
    private Long parId;
    @NotNull
    private Arbitro arbitro;
    @NotNull
    private Cancha cancha;
    @NotNull
    private Ronda ronda;
    private String descripcion;
    private Date fecha;
    private Long nivel;
    private Long tiempo;
    private Set<PartidoJugador> partidoJugadors = new HashSet<PartidoJugador>(0);

    public Partido() {
    }

    public Partido(Long parId, Arbitro arbitro, Cancha cancha,
        String descripcion, Date fecha, Long nivel,
        Set<PartidoJugador> partidoJugadors, Ronda ronda, Long tiempo) {
        this.parId = parId;
        this.arbitro = arbitro;
        this.cancha = cancha;
        this.ronda = ronda;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.nivel = nivel;
        this.tiempo = tiempo;
        this.partidoJugadors = partidoJugadors;
    }

    @Id
    @Column(name = "par_id", unique = true, nullable = false)
    public Long getParId() {
        return this.parId;
    }

    public void setParId(Long parId) {
        this.parId = parId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "arb_id")
    public Arbitro getArbitro() {
        return this.arbitro;
    }

    public void setArbitro(Arbitro arbitro) {
        this.arbitro = arbitro;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "can_id")
    public Cancha getCancha() {
        return this.cancha;
    }

    public void setCancha(Cancha cancha) {
        this.cancha = cancha;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ron_id")
    public Ronda getRonda() {
        return this.ronda;
    }

    public void setRonda(Ronda ronda) {
        this.ronda = ronda;
    }

    @Column(name = "descripcion")
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "fecha")
    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Column(name = "nivel")
    public Long getNivel() {
        return this.nivel;
    }

    public void setNivel(Long nivel) {
        this.nivel = nivel;
    }

    @Column(name = "tiempo")
    public Long getTiempo() {
        return this.tiempo;
    }

    public void setTiempo(Long tiempo) {
        this.tiempo = tiempo;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "partido")
    public Set<PartidoJugador> getPartidoJugadors() {
        return this.partidoJugadors;
    }

    public void setPartidoJugadors(Set<PartidoJugador> partidoJugadors) {
        this.partidoJugadors = partidoJugadors;
    }
}
