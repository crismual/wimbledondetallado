package co.edu.usbcali.wimbledon.modelo.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.util.Date;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public class RondaDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(RondaDTO.class);
    private Long nivel;
    private String nombre;
    private Long ronId;
    private Long dirId_Director;
    private Long torId_Torneo;

    public Long getNivel() {
        return nivel;
    }

    public void setNivel(Long nivel) {
        this.nivel = nivel;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getRonId() {
        return ronId;
    }

    public void setRonId(Long ronId) {
        this.ronId = ronId;
    }

    public Long getDirId_Director() {
        return dirId_Director;
    }

    public void setDirId_Director(Long dirId_Director) {
        this.dirId_Director = dirId_Director;
    }

    public Long getTorId_Torneo() {
        return torId_Torneo;
    }

    public void setTorId_Torneo(Long torId_Torneo) {
        this.torId_Torneo = torId_Torneo;
    }
}
