package co.edu.usbcali.wimbledon.modelo;

import org.hibernate.validator.constraints.*;

import java.util.Date;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "partido_jugador", schema = "public")
public class PartidoJugador implements java.io.Serializable {
    @NotNull
    private Long parjugId;
    @NotNull
    private Jugador jugador;
    @NotNull
    private Partido partido;
    private Long puntaje;

    public PartidoJugador() {
    }

    public PartidoJugador(Long parjugId, Jugador jugador, Partido partido,
        Long puntaje) {
        this.parjugId = parjugId;
        this.jugador = jugador;
        this.partido = partido;
        this.puntaje = puntaje;
    }

    @Id
    @Column(name = "parjug_id", unique = true, nullable = false)
    public Long getParjugId() {
        return this.parjugId;
    }

    public void setParjugId(Long parjugId) {
        this.parjugId = parjugId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "jug_id")
    public Jugador getJugador() {
        return this.jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "par_id")
    public Partido getPartido() {
        return this.partido;
    }

    public void setPartido(Partido partido) {
        this.partido = partido;
    }

    @Column(name = "puntaje")
    public Long getPuntaje() {
        return this.puntaje;
    }

    public void setPuntaje(Long puntaje) {
        this.puntaje = puntaje;
    }
}
