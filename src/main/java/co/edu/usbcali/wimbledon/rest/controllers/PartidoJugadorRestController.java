package co.edu.usbcali.wimbledon.rest.controllers;

import co.edu.usbcali.wimbledon.dto.mapper.IPartidoJugadorMapper;
import co.edu.usbcali.wimbledon.modelo.*;
import co.edu.usbcali.wimbledon.modelo.dto.PartidoJugadorDTO;
import co.edu.usbcali.wimbledon.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/partidoJugador")
public class PartidoJugadorRestController {
    private static final Logger log = LoggerFactory.getLogger(PartidoJugadorRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IPartidoJugadorMapper partidoJugadorMapper;

    @PostMapping(value = "/savePartidoJugador")
    public void savePartidoJugador(
        @RequestBody
    PartidoJugadorDTO partidoJugadorDTO) throws Exception {
        try {
            PartidoJugador partidoJugador = partidoJugadorMapper.partidoJugadorDTOToPartidoJugador(partidoJugadorDTO);

            businessDelegatorView.savePartidoJugador(partidoJugador);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deletePartidoJugador/{parjugId}")
    public void deletePartidoJugador(@PathVariable("parjugId")
    Long parjugId) throws Exception {
        try {
            PartidoJugador partidoJugador = businessDelegatorView.getPartidoJugador(parjugId);

            businessDelegatorView.deletePartidoJugador(partidoJugador);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updatePartidoJugador/")
    public void updatePartidoJugador(
        @RequestBody
    PartidoJugadorDTO partidoJugadorDTO) throws Exception {
        try {
            PartidoJugador partidoJugador = partidoJugadorMapper.partidoJugadorDTOToPartidoJugador(partidoJugadorDTO);

            businessDelegatorView.updatePartidoJugador(partidoJugador);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataPartidoJugador")
    public List<PartidoJugadorDTO> getDataPartidoJugador()
        throws Exception {
        try {
            return businessDelegatorView.getDataPartidoJugador();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getPartidoJugador/{parjugId}")
    public PartidoJugadorDTO getPartidoJugador(
        @PathVariable("parjugId")
    Long parjugId) throws Exception {
        try {
            PartidoJugador partidoJugador = businessDelegatorView.getPartidoJugador(parjugId);

            return partidoJugadorMapper.partidoJugadorToPartidoJugadorDTO(partidoJugador);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
