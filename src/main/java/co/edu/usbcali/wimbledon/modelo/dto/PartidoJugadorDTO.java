package co.edu.usbcali.wimbledon.modelo.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.util.Date;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public class PartidoJugadorDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(PartidoJugadorDTO.class);
    private Long parjugId;
    private Long puntaje;
    private Long jugId_Jugador;
    private Long parId_Partido;

    public Long getParjugId() {
        return parjugId;
    }

    public void setParjugId(Long parjugId) {
        this.parjugId = parjugId;
    }

    public Long getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(Long puntaje) {
        this.puntaje = puntaje;
    }

    public Long getJugId_Jugador() {
        return jugId_Jugador;
    }

    public void setJugId_Jugador(Long jugId_Jugador) {
        this.jugId_Jugador = jugId_Jugador;
    }

    public Long getParId_Partido() {
        return parId_Partido;
    }

    public void setParId_Partido(Long parId_Partido) {
        this.parId_Partido = parId_Partido;
    }
}
