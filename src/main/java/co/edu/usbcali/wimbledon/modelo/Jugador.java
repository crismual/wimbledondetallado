package co.edu.usbcali.wimbledon.modelo;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "jugador", schema = "public")
public class Jugador implements java.io.Serializable {
    @NotNull
    private Long jugId;
    private String apellido;
    private Long cedula;
    private String correo;
    private Long estatura;
    private Date fechaNacimiento;
    private String nombre;
    private Long peso;
    private Long ranking;
    private Set<PartidoJugador> partidoJugadors = new HashSet<PartidoJugador>(0);

    public Jugador() {
    }

    public Jugador(Long jugId, String apellido, Long cedula, String correo,
        Long estatura, Date fechaNacimiento, String nombre,
        Set<PartidoJugador> partidoJugadors, Long peso, Long ranking) {
        this.jugId = jugId;
        this.apellido = apellido;
        this.cedula = cedula;
        this.correo = correo;
        this.estatura = estatura;
        this.fechaNacimiento = fechaNacimiento;
        this.nombre = nombre;
        this.peso = peso;
        this.ranking = ranking;
        this.partidoJugadors = partidoJugadors;
    }

    @Id
    @Column(name = "jug_id", unique = true, nullable = false)
    public Long getJugId() {
        return this.jugId;
    }

    public void setJugId(Long jugId) {
        this.jugId = jugId;
    }

    @Column(name = "apellido")
    public String getApellido() {
        return this.apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Column(name = "cedula")
    public Long getCedula() {
        return this.cedula;
    }

    public void setCedula(Long cedula) {
        this.cedula = cedula;
    }

    @Column(name = "correo")
    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Column(name = "estatura")
    public Long getEstatura() {
        return this.estatura;
    }

    public void setEstatura(Long estatura) {
        this.estatura = estatura;
    }

    @Column(name = "fecha_nacimiento")
    public Date getFechaNacimiento() {
        return this.fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Column(name = "nombre")
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "peso")
    public Long getPeso() {
        return this.peso;
    }

    public void setPeso(Long peso) {
        this.peso = peso;
    }

    @Column(name = "ranking")
    public Long getRanking() {
        return this.ranking;
    }

    public void setRanking(Long ranking) {
        this.ranking = ranking;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "jugador")
    public Set<PartidoJugador> getPartidoJugadors() {
        return this.partidoJugadors;
    }

    public void setPartidoJugadors(Set<PartidoJugador> partidoJugadors) {
        this.partidoJugadors = partidoJugadors;
    }
}
