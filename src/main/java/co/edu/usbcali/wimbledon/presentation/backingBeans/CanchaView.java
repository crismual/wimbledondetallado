package co.edu.usbcali.wimbledon.presentation.backingBeans;

import co.edu.usbcali.wimbledon.exceptions.*;
import co.edu.usbcali.wimbledon.modelo.*;
import co.edu.usbcali.wimbledon.modelo.dto.CanchaDTO;
import co.edu.usbcali.wimbledon.presentation.businessDelegate.*;
import co.edu.usbcali.wimbledon.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class CanchaView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(CanchaView.class);
    private InputText txtEstado;
    private InputText txtNombre;
    private InputText txtUbicacion;
    private InputText txtCanId;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<CanchaDTO> data;
    private CanchaDTO selectedCancha;
    private Cancha entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public CanchaView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedCancha = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedCancha = null;

        if (txtEstado != null) {
            txtEstado.setValue(null);
            txtEstado.setDisabled(true);
        }

        if (txtNombre != null) {
            txtNombre.setValue(null);
            txtNombre.setDisabled(true);
        }

        if (txtUbicacion != null) {
            txtUbicacion.setValue(null);
            txtUbicacion.setDisabled(true);
        }

        if (txtCanId != null) {
            txtCanId.setValue(null);
            txtCanId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtId() {
        try {
            Long canId = FacesUtils.checkLong(txtCanId);
            entity = (canId != null) ? businessDelegatorView.getCancha(canId)
                                     : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtEstado.setDisabled(false);
            txtNombre.setDisabled(false);
            txtUbicacion.setDisabled(false);
            txtCanId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtEstado.setValue(entity.getEstado());
            txtEstado.setDisabled(false);
            txtNombre.setValue(entity.getNombre());
            txtNombre.setDisabled(false);
            txtUbicacion.setValue(entity.getUbicacion());
            txtUbicacion.setDisabled(false);
            txtCanId.setValue(entity.getCanId());
            txtCanId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedCancha = (CanchaDTO) (evt.getComponent().getAttributes()
                                         .get("selectedCancha"));
        txtEstado.setValue(selectedCancha.getEstado());
        txtEstado.setDisabled(false);
        txtNombre.setValue(selectedCancha.getNombre());
        txtNombre.setDisabled(false);
        txtUbicacion.setValue(selectedCancha.getUbicacion());
        txtUbicacion.setDisabled(false);
        txtCanId.setValue(selectedCancha.getCanId());
        txtCanId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedCancha == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Cancha();

            Long canId = FacesUtils.checkLong(txtCanId);

            entity.setCanId(canId);
            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setNombre(FacesUtils.checkString(txtNombre));
            entity.setUbicacion(FacesUtils.checkString(txtUbicacion));
            businessDelegatorView.saveCancha(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long canId = new Long(selectedCancha.getCanId());
                entity = businessDelegatorView.getCancha(canId);
            }

            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setNombre(FacesUtils.checkString(txtNombre));
            entity.setUbicacion(FacesUtils.checkString(txtUbicacion));
            businessDelegatorView.updateCancha(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedCancha = (CanchaDTO) (evt.getComponent().getAttributes()
                                             .get("selectedCancha"));

            Long canId = new Long(selectedCancha.getCanId());
            entity = businessDelegatorView.getCancha(canId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long canId = FacesUtils.checkLong(txtCanId);
            entity = businessDelegatorView.getCancha(canId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteCancha(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(Long canId, String estado, String nombre,
        String ubicacion) throws Exception {
        try {
            entity.setEstado(FacesUtils.checkString(estado));
            entity.setNombre(FacesUtils.checkString(nombre));
            entity.setUbicacion(FacesUtils.checkString(ubicacion));
            businessDelegatorView.updateCancha(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("CanchaView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtEstado() {
        return txtEstado;
    }

    public void setTxtEstado(InputText txtEstado) {
        this.txtEstado = txtEstado;
    }

    public InputText getTxtNombre() {
        return txtNombre;
    }

    public void setTxtNombre(InputText txtNombre) {
        this.txtNombre = txtNombre;
    }

    public InputText getTxtUbicacion() {
        return txtUbicacion;
    }

    public void setTxtUbicacion(InputText txtUbicacion) {
        this.txtUbicacion = txtUbicacion;
    }

    public InputText getTxtCanId() {
        return txtCanId;
    }

    public void setTxtCanId(InputText txtCanId) {
        this.txtCanId = txtCanId;
    }

    public List<CanchaDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataCancha();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<CanchaDTO> canchaDTO) {
        this.data = canchaDTO;
    }

    public CanchaDTO getSelectedCancha() {
        return selectedCancha;
    }

    public void setSelectedCancha(CanchaDTO cancha) {
        this.selectedCancha = cancha;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}
