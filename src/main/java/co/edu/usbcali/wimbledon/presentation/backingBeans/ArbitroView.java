package co.edu.usbcali.wimbledon.presentation.backingBeans;

import co.edu.usbcali.wimbledon.exceptions.*;
import co.edu.usbcali.wimbledon.modelo.*;
import co.edu.usbcali.wimbledon.modelo.dto.ArbitroDTO;
import co.edu.usbcali.wimbledon.presentation.businessDelegate.*;
import co.edu.usbcali.wimbledon.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class ArbitroView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(ArbitroView.class);
    private InputText txtApellido;
    private InputText txtCedula;
    private InputText txtCorreo;
    private InputText txtEstatura;
    private InputText txtNombre;
    private InputText txtPeso;
    private InputText txtArbId;
    private Calendar txtFechaNacimiento;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<ArbitroDTO> data;
    private ArbitroDTO selectedArbitro;
    private Arbitro entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public ArbitroView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedArbitro = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedArbitro = null;

        if (txtApellido != null) {
            txtApellido.setValue(null);
            txtApellido.setDisabled(true);
        }

        if (txtCedula != null) {
            txtCedula.setValue(null);
            txtCedula.setDisabled(true);
        }

        if (txtCorreo != null) {
            txtCorreo.setValue(null);
            txtCorreo.setDisabled(true);
        }

        if (txtEstatura != null) {
            txtEstatura.setValue(null);
            txtEstatura.setDisabled(true);
        }

        if (txtNombre != null) {
            txtNombre.setValue(null);
            txtNombre.setDisabled(true);
        }

        if (txtPeso != null) {
            txtPeso.setValue(null);
            txtPeso.setDisabled(true);
        }

        if (txtFechaNacimiento != null) {
            txtFechaNacimiento.setValue(null);
            txtFechaNacimiento.setDisabled(true);
        }

        if (txtArbId != null) {
            txtArbId.setValue(null);
            txtArbId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFechaNacimiento() {
        Date inputDate = (Date) txtFechaNacimiento.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            Long arbId = FacesUtils.checkLong(txtArbId);
            entity = (arbId != null) ? businessDelegatorView.getArbitro(arbId)
                                     : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtApellido.setDisabled(false);
            txtCedula.setDisabled(false);
            txtCorreo.setDisabled(false);
            txtEstatura.setDisabled(false);
            txtNombre.setDisabled(false);
            txtPeso.setDisabled(false);
            txtFechaNacimiento.setDisabled(false);
            txtArbId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtApellido.setValue(entity.getApellido());
            txtApellido.setDisabled(false);
            txtCedula.setValue(entity.getCedula());
            txtCedula.setDisabled(false);
            txtCorreo.setValue(entity.getCorreo());
            txtCorreo.setDisabled(false);
            txtEstatura.setValue(entity.getEstatura());
            txtEstatura.setDisabled(false);
            txtFechaNacimiento.setValue(entity.getFechaNacimiento());
            txtFechaNacimiento.setDisabled(false);
            txtNombre.setValue(entity.getNombre());
            txtNombre.setDisabled(false);
            txtPeso.setValue(entity.getPeso());
            txtPeso.setDisabled(false);
            txtArbId.setValue(entity.getArbId());
            txtArbId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedArbitro = (ArbitroDTO) (evt.getComponent().getAttributes()
                                           .get("selectedArbitro"));
        txtApellido.setValue(selectedArbitro.getApellido());
        txtApellido.setDisabled(false);
        txtCedula.setValue(selectedArbitro.getCedula());
        txtCedula.setDisabled(false);
        txtCorreo.setValue(selectedArbitro.getCorreo());
        txtCorreo.setDisabled(false);
        txtEstatura.setValue(selectedArbitro.getEstatura());
        txtEstatura.setDisabled(false);
        txtFechaNacimiento.setValue(selectedArbitro.getFechaNacimiento());
        txtFechaNacimiento.setDisabled(false);
        txtNombre.setValue(selectedArbitro.getNombre());
        txtNombre.setDisabled(false);
        txtPeso.setValue(selectedArbitro.getPeso());
        txtPeso.setDisabled(false);
        txtArbId.setValue(selectedArbitro.getArbId());
        txtArbId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedArbitro == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Arbitro();

            Long arbId = FacesUtils.checkLong(txtArbId);

            entity.setApellido(FacesUtils.checkString(txtApellido));
            entity.setArbId(arbId);
            entity.setCedula(FacesUtils.checkLong(txtCedula));
            entity.setCorreo(FacesUtils.checkString(txtCorreo));
            entity.setEstatura(FacesUtils.checkLong(txtEstatura));
            entity.setFechaNacimiento(FacesUtils.checkDate(txtFechaNacimiento));
            entity.setNombre(FacesUtils.checkString(txtNombre));
            entity.setPeso(FacesUtils.checkLong(txtPeso));
            businessDelegatorView.saveArbitro(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long arbId = new Long(selectedArbitro.getArbId());
                entity = businessDelegatorView.getArbitro(arbId);
            }

            entity.setApellido(FacesUtils.checkString(txtApellido));
            entity.setCedula(FacesUtils.checkLong(txtCedula));
            entity.setCorreo(FacesUtils.checkString(txtCorreo));
            entity.setEstatura(FacesUtils.checkLong(txtEstatura));
            entity.setFechaNacimiento(FacesUtils.checkDate(txtFechaNacimiento));
            entity.setNombre(FacesUtils.checkString(txtNombre));
            entity.setPeso(FacesUtils.checkLong(txtPeso));
            businessDelegatorView.updateArbitro(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedArbitro = (ArbitroDTO) (evt.getComponent().getAttributes()
                                               .get("selectedArbitro"));

            Long arbId = new Long(selectedArbitro.getArbId());
            entity = businessDelegatorView.getArbitro(arbId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long arbId = FacesUtils.checkLong(txtArbId);
            entity = businessDelegatorView.getArbitro(arbId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteArbitro(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String apellido, Long arbId, Long cedula,
        String correo, Long estatura, Date fechaNacimiento, String nombre,
        Long peso) throws Exception {
        try {
            entity.setApellido(FacesUtils.checkString(apellido));
            entity.setCedula(FacesUtils.checkLong(cedula));
            entity.setCorreo(FacesUtils.checkString(correo));
            entity.setEstatura(FacesUtils.checkLong(estatura));
            entity.setFechaNacimiento(FacesUtils.checkDate(fechaNacimiento));
            entity.setNombre(FacesUtils.checkString(nombre));
            entity.setPeso(FacesUtils.checkLong(peso));
            businessDelegatorView.updateArbitro(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("ArbitroView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtApellido() {
        return txtApellido;
    }

    public void setTxtApellido(InputText txtApellido) {
        this.txtApellido = txtApellido;
    }

    public InputText getTxtCedula() {
        return txtCedula;
    }

    public void setTxtCedula(InputText txtCedula) {
        this.txtCedula = txtCedula;
    }

    public InputText getTxtCorreo() {
        return txtCorreo;
    }

    public void setTxtCorreo(InputText txtCorreo) {
        this.txtCorreo = txtCorreo;
    }

    public InputText getTxtEstatura() {
        return txtEstatura;
    }

    public void setTxtEstatura(InputText txtEstatura) {
        this.txtEstatura = txtEstatura;
    }

    public InputText getTxtNombre() {
        return txtNombre;
    }

    public void setTxtNombre(InputText txtNombre) {
        this.txtNombre = txtNombre;
    }

    public InputText getTxtPeso() {
        return txtPeso;
    }

    public void setTxtPeso(InputText txtPeso) {
        this.txtPeso = txtPeso;
    }

    public Calendar getTxtFechaNacimiento() {
        return txtFechaNacimiento;
    }

    public void setTxtFechaNacimiento(Calendar txtFechaNacimiento) {
        this.txtFechaNacimiento = txtFechaNacimiento;
    }

    public InputText getTxtArbId() {
        return txtArbId;
    }

    public void setTxtArbId(InputText txtArbId) {
        this.txtArbId = txtArbId;
    }

    public List<ArbitroDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataArbitro();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<ArbitroDTO> arbitroDTO) {
        this.data = arbitroDTO;
    }

    public ArbitroDTO getSelectedArbitro() {
        return selectedArbitro;
    }

    public void setSelectedArbitro(ArbitroDTO arbitro) {
        this.selectedArbitro = arbitro;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}
