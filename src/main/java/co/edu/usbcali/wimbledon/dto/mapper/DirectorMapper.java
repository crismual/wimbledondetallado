package co.edu.usbcali.wimbledon.dto.mapper;

import co.edu.usbcali.wimbledon.modelo.*;
import co.edu.usbcali.wimbledon.modelo.Director;
import co.edu.usbcali.wimbledon.modelo.control.*;
import co.edu.usbcali.wimbledon.modelo.dto.DirectorDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class DirectorMapper implements IDirectorMapper {
    private static final Logger log = LoggerFactory.getLogger(DirectorMapper.class);

    @Transactional(readOnly = true)
    public DirectorDTO directorToDirectorDTO(Director director)
        throws Exception {
        try {
            DirectorDTO directorDTO = new DirectorDTO();

            directorDTO.setDirId(director.getDirId());
            directorDTO.setApellido((director.getApellido() != null)
                ? director.getApellido() : null);
            directorDTO.setCedula((director.getCedula() != null)
                ? director.getCedula() : null);
            directorDTO.setCorreo((director.getCorreo() != null)
                ? director.getCorreo() : null);
            directorDTO.setNombre((director.getNombre() != null)
                ? director.getNombre() : null);

            return directorDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Director directorDTOToDirector(DirectorDTO directorDTO)
        throws Exception {
        try {
            Director director = new Director();

            director.setDirId(directorDTO.getDirId());
            director.setApellido((directorDTO.getApellido() != null)
                ? directorDTO.getApellido() : null);
            director.setCedula((directorDTO.getCedula() != null)
                ? directorDTO.getCedula() : null);
            director.setCorreo((directorDTO.getCorreo() != null)
                ? directorDTO.getCorreo() : null);
            director.setNombre((directorDTO.getNombre() != null)
                ? directorDTO.getNombre() : null);

            return director;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<DirectorDTO> listDirectorToListDirectorDTO(
        List<Director> listDirector) throws Exception {
        try {
            List<DirectorDTO> directorDTOs = new ArrayList<DirectorDTO>();

            for (Director director : listDirector) {
                DirectorDTO directorDTO = directorToDirectorDTO(director);

                directorDTOs.add(directorDTO);
            }

            return directorDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Director> listDirectorDTOToListDirector(
        List<DirectorDTO> listDirectorDTO) throws Exception {
        try {
            List<Director> listDirector = new ArrayList<Director>();

            for (DirectorDTO directorDTO : listDirectorDTO) {
                Director director = directorDTOToDirector(directorDTO);

                listDirector.add(director);
            }

            return listDirector;
        } catch (Exception e) {
            throw e;
        }
    }
}
