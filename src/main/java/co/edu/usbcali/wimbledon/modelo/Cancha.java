package co.edu.usbcali.wimbledon.modelo;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "cancha", schema = "public")
public class Cancha implements java.io.Serializable {
    @NotNull
    private Long canId;
    private String estado;
    private String nombre;
    private String ubicacion;
    private Set<Partido> partidos = new HashSet<Partido>(0);

    public Cancha() {
    }

    public Cancha(Long canId, String estado, String nombre,
        Set<Partido> partidos, String ubicacion) {
        this.canId = canId;
        this.estado = estado;
        this.nombre = nombre;
        this.ubicacion = ubicacion;
        this.partidos = partidos;
    }

    @Id
    @Column(name = "can_id", unique = true, nullable = false)
    public Long getCanId() {
        return this.canId;
    }

    public void setCanId(Long canId) {
        this.canId = canId;
    }

    @Column(name = "estado")
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "nombre")
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "ubicacion")
    public String getUbicacion() {
        return this.ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cancha")
    public Set<Partido> getPartidos() {
        return this.partidos;
    }

    public void setPartidos(Set<Partido> partidos) {
        this.partidos = partidos;
    }
}
