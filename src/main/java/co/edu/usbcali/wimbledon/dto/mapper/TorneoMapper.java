package co.edu.usbcali.wimbledon.dto.mapper;

import co.edu.usbcali.wimbledon.modelo.*;
import co.edu.usbcali.wimbledon.modelo.Torneo;
import co.edu.usbcali.wimbledon.modelo.control.*;
import co.edu.usbcali.wimbledon.modelo.dto.TorneoDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class TorneoMapper implements ITorneoMapper {
    private static final Logger log = LoggerFactory.getLogger(TorneoMapper.class);

    @Transactional(readOnly = true)
    public TorneoDTO torneoToTorneoDTO(Torneo torneo) throws Exception {
        try {
            TorneoDTO torneoDTO = new TorneoDTO();

            torneoDTO.setTorId(torneo.getTorId());
            torneoDTO.setDescripcion((torneo.getDescripcion() != null)
                ? torneo.getDescripcion() : null);
            torneoDTO.setNombre((torneo.getNombre() != null)
                ? torneo.getNombre() : null);

            return torneoDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Torneo torneoDTOToTorneo(TorneoDTO torneoDTO)
        throws Exception {
        try {
            Torneo torneo = new Torneo();

            torneo.setTorId(torneoDTO.getTorId());
            torneo.setDescripcion((torneoDTO.getDescripcion() != null)
                ? torneoDTO.getDescripcion() : null);
            torneo.setNombre((torneoDTO.getNombre() != null)
                ? torneoDTO.getNombre() : null);

            return torneo;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<TorneoDTO> listTorneoToListTorneoDTO(List<Torneo> listTorneo)
        throws Exception {
        try {
            List<TorneoDTO> torneoDTOs = new ArrayList<TorneoDTO>();

            for (Torneo torneo : listTorneo) {
                TorneoDTO torneoDTO = torneoToTorneoDTO(torneo);

                torneoDTOs.add(torneoDTO);
            }

            return torneoDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Torneo> listTorneoDTOToListTorneo(List<TorneoDTO> listTorneoDTO)
        throws Exception {
        try {
            List<Torneo> listTorneo = new ArrayList<Torneo>();

            for (TorneoDTO torneoDTO : listTorneoDTO) {
                Torneo torneo = torneoDTOToTorneo(torneoDTO);

                listTorneo.add(torneo);
            }

            return listTorneo;
        } catch (Exception e) {
            throw e;
        }
    }
}
