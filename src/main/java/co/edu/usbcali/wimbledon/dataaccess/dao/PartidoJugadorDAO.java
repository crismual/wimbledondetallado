package co.edu.usbcali.wimbledon.dataaccess.dao;

import co.edu.usbcali.wimbledon.dataaccess.api.JpaDaoImpl;
import co.edu.usbcali.wimbledon.modelo.PartidoJugador;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 * A data access object (DAO) providing persistence and search support for
 * PartidoJugador entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 *
 * @see lidis.PartidoJugador
 */
@Scope("singleton")
@Repository("PartidoJugadorDAO")
public class PartidoJugadorDAO extends JpaDaoImpl<PartidoJugador, Long>
    implements IPartidoJugadorDAO {
    private static final Logger log = LoggerFactory.getLogger(PartidoJugadorDAO.class);
    @PersistenceContext
    private EntityManager entityManager;

    public static IPartidoJugadorDAO getFromApplicationContext(
        ApplicationContext ctx) {
        return (IPartidoJugadorDAO) ctx.getBean("PartidoJugadorDAO");
    }
}
