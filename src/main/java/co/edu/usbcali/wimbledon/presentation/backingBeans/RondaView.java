package co.edu.usbcali.wimbledon.presentation.backingBeans;

import co.edu.usbcali.wimbledon.exceptions.*;
import co.edu.usbcali.wimbledon.modelo.*;
import co.edu.usbcali.wimbledon.modelo.dto.RondaDTO;
import co.edu.usbcali.wimbledon.presentation.businessDelegate.*;
import co.edu.usbcali.wimbledon.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class RondaView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(RondaView.class);
    private InputText txtNivel;
    private InputText txtNombre;
    private InputText txtDirId_Director;
    private InputText txtTorId_Torneo;
    private InputText txtRonId;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<RondaDTO> data;
    private RondaDTO selectedRonda;
    private Ronda entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public RondaView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedRonda = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedRonda = null;

        if (txtNivel != null) {
            txtNivel.setValue(null);
            txtNivel.setDisabled(true);
        }

        if (txtNombre != null) {
            txtNombre.setValue(null);
            txtNombre.setDisabled(true);
        }

        if (txtDirId_Director != null) {
            txtDirId_Director.setValue(null);
            txtDirId_Director.setDisabled(true);
        }

        if (txtTorId_Torneo != null) {
            txtTorId_Torneo.setValue(null);
            txtTorId_Torneo.setDisabled(true);
        }

        if (txtRonId != null) {
            txtRonId.setValue(null);
            txtRonId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtId() {
        try {
            Long ronId = FacesUtils.checkLong(txtRonId);
            entity = (ronId != null) ? businessDelegatorView.getRonda(ronId)
                                     : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtNivel.setDisabled(false);
            txtNombre.setDisabled(false);
            txtDirId_Director.setDisabled(false);
            txtTorId_Torneo.setDisabled(false);
            txtRonId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtNivel.setValue(entity.getNivel());
            txtNivel.setDisabled(false);
            txtNombre.setValue(entity.getNombre());
            txtNombre.setDisabled(false);
            txtDirId_Director.setValue(entity.getDirector().getDirId());
            txtDirId_Director.setDisabled(false);
            txtTorId_Torneo.setValue(entity.getTorneo().getTorId());
            txtTorId_Torneo.setDisabled(false);
            txtRonId.setValue(entity.getRonId());
            txtRonId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedRonda = (RondaDTO) (evt.getComponent().getAttributes()
                                       .get("selectedRonda"));
        txtNivel.setValue(selectedRonda.getNivel());
        txtNivel.setDisabled(false);
        txtNombre.setValue(selectedRonda.getNombre());
        txtNombre.setDisabled(false);
        txtDirId_Director.setValue(selectedRonda.getDirId_Director());
        txtDirId_Director.setDisabled(false);
        txtTorId_Torneo.setValue(selectedRonda.getTorId_Torneo());
        txtTorId_Torneo.setDisabled(false);
        txtRonId.setValue(selectedRonda.getRonId());
        txtRonId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedRonda == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Ronda();

            Long ronId = FacesUtils.checkLong(txtRonId);

            entity.setNivel(FacesUtils.checkLong(txtNivel));
            entity.setNombre(FacesUtils.checkString(txtNombre));
            entity.setRonId(ronId);
            entity.setDirector((FacesUtils.checkLong(txtDirId_Director) != null)
                ? businessDelegatorView.getDirector(FacesUtils.checkLong(
                        txtDirId_Director)) : null);
            entity.setTorneo((FacesUtils.checkLong(txtTorId_Torneo) != null)
                ? businessDelegatorView.getTorneo(FacesUtils.checkLong(
                        txtTorId_Torneo)) : null);
            businessDelegatorView.saveRonda(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long ronId = new Long(selectedRonda.getRonId());
                entity = businessDelegatorView.getRonda(ronId);
            }

            entity.setNivel(FacesUtils.checkLong(txtNivel));
            entity.setNombre(FacesUtils.checkString(txtNombre));
            entity.setDirector((FacesUtils.checkLong(txtDirId_Director) != null)
                ? businessDelegatorView.getDirector(FacesUtils.checkLong(
                        txtDirId_Director)) : null);
            entity.setTorneo((FacesUtils.checkLong(txtTorId_Torneo) != null)
                ? businessDelegatorView.getTorneo(FacesUtils.checkLong(
                        txtTorId_Torneo)) : null);
            businessDelegatorView.updateRonda(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedRonda = (RondaDTO) (evt.getComponent().getAttributes()
                                           .get("selectedRonda"));

            Long ronId = new Long(selectedRonda.getRonId());
            entity = businessDelegatorView.getRonda(ronId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long ronId = FacesUtils.checkLong(txtRonId);
            entity = businessDelegatorView.getRonda(ronId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteRonda(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(Long nivel, String nombre, Long ronId,
        Long dirId_Director, Long torId_Torneo) throws Exception {
        try {
            entity.setNivel(FacesUtils.checkLong(nivel));
            entity.setNombre(FacesUtils.checkString(nombre));
            businessDelegatorView.updateRonda(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("RondaView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtNivel() {
        return txtNivel;
    }

    public void setTxtNivel(InputText txtNivel) {
        this.txtNivel = txtNivel;
    }

    public InputText getTxtNombre() {
        return txtNombre;
    }

    public void setTxtNombre(InputText txtNombre) {
        this.txtNombre = txtNombre;
    }

    public InputText getTxtDirId_Director() {
        return txtDirId_Director;
    }

    public void setTxtDirId_Director(InputText txtDirId_Director) {
        this.txtDirId_Director = txtDirId_Director;
    }

    public InputText getTxtTorId_Torneo() {
        return txtTorId_Torneo;
    }

    public void setTxtTorId_Torneo(InputText txtTorId_Torneo) {
        this.txtTorId_Torneo = txtTorId_Torneo;
    }

    public InputText getTxtRonId() {
        return txtRonId;
    }

    public void setTxtRonId(InputText txtRonId) {
        this.txtRonId = txtRonId;
    }

    public List<RondaDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataRonda();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<RondaDTO> rondaDTO) {
        this.data = rondaDTO;
    }

    public RondaDTO getSelectedRonda() {
        return selectedRonda;
    }

    public void setSelectedRonda(RondaDTO ronda) {
        this.selectedRonda = ronda;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}
