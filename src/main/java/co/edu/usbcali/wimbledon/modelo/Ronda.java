package co.edu.usbcali.wimbledon.modelo;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "ronda", schema = "public")
public class Ronda implements java.io.Serializable {
    @NotNull
    private Long ronId;
    @NotNull
    private Director director;
    @NotNull
    private Torneo torneo;
    private Long nivel;
    private String nombre;
    private Set<Partido> partidos = new HashSet<Partido>(0);

    public Ronda() {
    }

    public Ronda(Long ronId, Director director, Long nivel, String nombre,
        Set<Partido> partidos, Torneo torneo) {
        this.ronId = ronId;
        this.director = director;
        this.torneo = torneo;
        this.nivel = nivel;
        this.nombre = nombre;
        this.partidos = partidos;
    }

    @Id
    @Column(name = "ron_id", unique = true, nullable = false)
    public Long getRonId() {
        return this.ronId;
    }

    public void setRonId(Long ronId) {
        this.ronId = ronId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dir_id")
    public Director getDirector() {
        return this.director;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tor_id")
    public Torneo getTorneo() {
        return this.torneo;
    }

    public void setTorneo(Torneo torneo) {
        this.torneo = torneo;
    }

    @Column(name = "nivel")
    public Long getNivel() {
        return this.nivel;
    }

    public void setNivel(Long nivel) {
        this.nivel = nivel;
    }

    @Column(name = "nombre")
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ronda")
    public Set<Partido> getPartidos() {
        return this.partidos;
    }

    public void setPartidos(Set<Partido> partidos) {
        this.partidos = partidos;
    }
}
