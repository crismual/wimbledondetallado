package co.edu.usbcali.wimbledon.dto.mapper;

import co.edu.usbcali.wimbledon.modelo.*;
import co.edu.usbcali.wimbledon.modelo.PartidoJugador;
import co.edu.usbcali.wimbledon.modelo.control.*;
import co.edu.usbcali.wimbledon.modelo.dto.PartidoJugadorDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class PartidoJugadorMapper implements IPartidoJugadorMapper {
    private static final Logger log = LoggerFactory.getLogger(PartidoJugadorMapper.class);

    /**
    * Logic injected by Spring that manages Jugador entities
    *
    */
    @Autowired
    IJugadorLogic logicJugador1;

    /**
    * Logic injected by Spring that manages Partido entities
    *
    */
    @Autowired
    IPartidoLogic logicPartido2;

    @Transactional(readOnly = true)
    public PartidoJugadorDTO partidoJugadorToPartidoJugadorDTO(
        PartidoJugador partidoJugador) throws Exception {
        try {
            PartidoJugadorDTO partidoJugadorDTO = new PartidoJugadorDTO();

            partidoJugadorDTO.setParjugId(partidoJugador.getParjugId());
            partidoJugadorDTO.setPuntaje((partidoJugador.getPuntaje() != null)
                ? partidoJugador.getPuntaje() : null);
            partidoJugadorDTO.setJugId_Jugador((partidoJugador.getJugador()
                                                              .getJugId() != null)
                ? partidoJugador.getJugador().getJugId() : null);
            partidoJugadorDTO.setParId_Partido((partidoJugador.getPartido()
                                                              .getParId() != null)
                ? partidoJugador.getPartido().getParId() : null);

            return partidoJugadorDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public PartidoJugador partidoJugadorDTOToPartidoJugador(
        PartidoJugadorDTO partidoJugadorDTO) throws Exception {
        try {
            PartidoJugador partidoJugador = new PartidoJugador();

            partidoJugador.setParjugId(partidoJugadorDTO.getParjugId());
            partidoJugador.setPuntaje((partidoJugadorDTO.getPuntaje() != null)
                ? partidoJugadorDTO.getPuntaje() : null);

            Jugador jugador = new Jugador();

            if (partidoJugadorDTO.getJugId_Jugador() != null) {
                jugador = logicJugador1.getJugador(partidoJugadorDTO.getJugId_Jugador());
            }

            if (jugador != null) {
                partidoJugador.setJugador(jugador);
            }

            Partido partido = new Partido();

            if (partidoJugadorDTO.getParId_Partido() != null) {
                partido = logicPartido2.getPartido(partidoJugadorDTO.getParId_Partido());
            }

            if (partido != null) {
                partidoJugador.setPartido(partido);
            }

            return partidoJugador;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<PartidoJugadorDTO> listPartidoJugadorToListPartidoJugadorDTO(
        List<PartidoJugador> listPartidoJugador) throws Exception {
        try {
            List<PartidoJugadorDTO> partidoJugadorDTOs = new ArrayList<PartidoJugadorDTO>();

            for (PartidoJugador partidoJugador : listPartidoJugador) {
                PartidoJugadorDTO partidoJugadorDTO = partidoJugadorToPartidoJugadorDTO(partidoJugador);

                partidoJugadorDTOs.add(partidoJugadorDTO);
            }

            return partidoJugadorDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<PartidoJugador> listPartidoJugadorDTOToListPartidoJugador(
        List<PartidoJugadorDTO> listPartidoJugadorDTO)
        throws Exception {
        try {
            List<PartidoJugador> listPartidoJugador = new ArrayList<PartidoJugador>();

            for (PartidoJugadorDTO partidoJugadorDTO : listPartidoJugadorDTO) {
                PartidoJugador partidoJugador = partidoJugadorDTOToPartidoJugador(partidoJugadorDTO);

                listPartidoJugador.add(partidoJugador);
            }

            return listPartidoJugador;
        } catch (Exception e) {
            throw e;
        }
    }
}
