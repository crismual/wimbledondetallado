package co.edu.usbcali.wimbledon.dto.mapper;

import co.edu.usbcali.wimbledon.modelo.*;
import co.edu.usbcali.wimbledon.modelo.Arbitro;
import co.edu.usbcali.wimbledon.modelo.control.*;
import co.edu.usbcali.wimbledon.modelo.dto.ArbitroDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class ArbitroMapper implements IArbitroMapper {
    private static final Logger log = LoggerFactory.getLogger(ArbitroMapper.class);

    @Transactional(readOnly = true)
    public ArbitroDTO arbitroToArbitroDTO(Arbitro arbitro)
        throws Exception {
        try {
            ArbitroDTO arbitroDTO = new ArbitroDTO();

            arbitroDTO.setArbId(arbitro.getArbId());
            arbitroDTO.setApellido((arbitro.getApellido() != null)
                ? arbitro.getApellido() : null);
            arbitroDTO.setCedula((arbitro.getCedula() != null)
                ? arbitro.getCedula() : null);
            arbitroDTO.setCorreo((arbitro.getCorreo() != null)
                ? arbitro.getCorreo() : null);
            arbitroDTO.setEstatura((arbitro.getEstatura() != null)
                ? arbitro.getEstatura() : null);
            arbitroDTO.setFechaNacimiento(arbitro.getFechaNacimiento());
            arbitroDTO.setNombre((arbitro.getNombre() != null)
                ? arbitro.getNombre() : null);
            arbitroDTO.setPeso((arbitro.getPeso() != null) ? arbitro.getPeso()
                                                           : null);

            return arbitroDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Arbitro arbitroDTOToArbitro(ArbitroDTO arbitroDTO)
        throws Exception {
        try {
            Arbitro arbitro = new Arbitro();

            arbitro.setArbId(arbitroDTO.getArbId());
            arbitro.setApellido((arbitroDTO.getApellido() != null)
                ? arbitroDTO.getApellido() : null);
            arbitro.setCedula((arbitroDTO.getCedula() != null)
                ? arbitroDTO.getCedula() : null);
            arbitro.setCorreo((arbitroDTO.getCorreo() != null)
                ? arbitroDTO.getCorreo() : null);
            arbitro.setEstatura((arbitroDTO.getEstatura() != null)
                ? arbitroDTO.getEstatura() : null);
            arbitro.setFechaNacimiento(arbitroDTO.getFechaNacimiento());
            arbitro.setNombre((arbitroDTO.getNombre() != null)
                ? arbitroDTO.getNombre() : null);
            arbitro.setPeso((arbitroDTO.getPeso() != null)
                ? arbitroDTO.getPeso() : null);

            return arbitro;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<ArbitroDTO> listArbitroToListArbitroDTO(
        List<Arbitro> listArbitro) throws Exception {
        try {
            List<ArbitroDTO> arbitroDTOs = new ArrayList<ArbitroDTO>();

            for (Arbitro arbitro : listArbitro) {
                ArbitroDTO arbitroDTO = arbitroToArbitroDTO(arbitro);

                arbitroDTOs.add(arbitroDTO);
            }

            return arbitroDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Arbitro> listArbitroDTOToListArbitro(
        List<ArbitroDTO> listArbitroDTO) throws Exception {
        try {
            List<Arbitro> listArbitro = new ArrayList<Arbitro>();

            for (ArbitroDTO arbitroDTO : listArbitroDTO) {
                Arbitro arbitro = arbitroDTOToArbitro(arbitroDTO);

                listArbitro.add(arbitro);
            }

            return listArbitro;
        } catch (Exception e) {
            throw e;
        }
    }
}
