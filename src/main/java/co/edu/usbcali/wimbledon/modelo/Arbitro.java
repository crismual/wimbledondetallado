package co.edu.usbcali.wimbledon.modelo;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "arbitro", schema = "public")
public class Arbitro implements java.io.Serializable {
    @NotNull
    private Long arbId;
    private String apellido;
    private Long cedula;
    private String correo;
    private Long estatura;
    private Date fechaNacimiento;
    private String nombre;
    private Long peso;
    private Set<Partido> partidos = new HashSet<Partido>(0);

    public Arbitro() {
    }

    public Arbitro(Long arbId, String apellido, Long cedula, String correo,
        Long estatura, Date fechaNacimiento, String nombre,
        Set<Partido> partidos, Long peso) {
        this.arbId = arbId;
        this.apellido = apellido;
        this.cedula = cedula;
        this.correo = correo;
        this.estatura = estatura;
        this.fechaNacimiento = fechaNacimiento;
        this.nombre = nombre;
        this.peso = peso;
        this.partidos = partidos;
    }

    @Id
    @Column(name = "arb_id", unique = true, nullable = false)
    public Long getArbId() {
        return this.arbId;
    }

    public void setArbId(Long arbId) {
        this.arbId = arbId;
    }

    @Column(name = "apellido")
    public String getApellido() {
        return this.apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Column(name = "cedula")
    public Long getCedula() {
        return this.cedula;
    }

    public void setCedula(Long cedula) {
        this.cedula = cedula;
    }

    @Column(name = "correo")
    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Column(name = "estatura")
    public Long getEstatura() {
        return this.estatura;
    }

    public void setEstatura(Long estatura) {
        this.estatura = estatura;
    }

    @Column(name = "fecha_nacimiento")
    public Date getFechaNacimiento() {
        return this.fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Column(name = "nombre")
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "peso")
    public Long getPeso() {
        return this.peso;
    }

    public void setPeso(Long peso) {
        this.peso = peso;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "arbitro")
    public Set<Partido> getPartidos() {
        return this.partidos;
    }

    public void setPartidos(Set<Partido> partidos) {
        this.partidos = partidos;
    }
}
