package co.edu.usbcali.wimbledon.presentation.backingBeans;

import co.edu.usbcali.wimbledon.exceptions.*;
import co.edu.usbcali.wimbledon.modelo.Jugador;
import co.edu.usbcali.wimbledon.presentation.businessDelegate.*;
import co.edu.usbcali.wimbledon.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import co.edu.usbcali.wimbledon.presentation.businessDelegate.IBusinessDelegatorView;

@ManagedBean
@ViewScoped
public class realizarSorteoView {
	
	private static final Logger log = LoggerFactory.getLogger(realizarSorteoView.class);

    private List<Jugador> losJugadores;


    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;
    
    
    public realizarSorteoView() {
    	super();
    }
    
    @PostConstruct
    public void init() {
    
    	try {
    		
    		losJugadores = businessDelegatorView.getJugador();
		} catch (Exception e) {
			// TODO: handle exception
		}
    	
    	
    }

    public String realizarSorteo() throws Exception {
    	
    	
    	List<Jugador> jugadoresTorneo = new ArrayList<>();
    	
    	jugadoresTorneo = businessDelegatorView.getJugador();
    	
    	List<Jugador> jugadoresSuperiores = new ArrayList<>();
    	List<Jugador> jugadoresInferiores = new ArrayList<>();
    	
    	List<Jugador> jugadoresPrimerCuartoSuperior = new ArrayList<>();
    	List<Jugador> jugadoresSegundoCuartoSuperior = new ArrayList<>();
    	List<Jugador> jugadoresPrimerCuartoInferior = new ArrayList<>();
    	List<Jugador> jugadoresSegundoCuartoInferior = new ArrayList<>();
    	
    	if (jugadoresTorneo == null) {
    		
    		FacesUtils.addErrorMessage("no hay jugadores en base de datos");
    		
		}
    	
    	for (Jugador jugador : jugadoresTorneo) {
			
    		if (jugador.getRanking() <= 5) {
				jugadoresInferiores.add(jugador);
			}else {
				jugadoresSuperiores.add(jugador);
			}
		}
    	
    	while (jugadoresSuperiores.size() != jugadoresInferiores.size()) {
    	
			if (jugadoresSuperiores.size() < jugadoresInferiores.size()) {
				
				Jugador JugadorInfToSup = new Jugador();
				JugadorInfToSup.setRanking(0L);
				
				for (Jugador jugador : jugadoresInferiores) {
					if (jugador.getRanking() > JugadorInfToSup.getRanking()) {
						JugadorInfToSup = jugador;
					}
				}
				
				jugadoresSuperiores.add(JugadorInfToSup);
				jugadoresInferiores.remove(JugadorInfToSup);

			}
			
			if (jugadoresInferiores.size() < jugadoresSuperiores.size()) {
				
				Jugador JugadorSupToInf = new Jugador();
				JugadorSupToInf.setRanking(10L);
				
				for (Jugador jugador : jugadoresSuperiores) {
					if (jugador.getRanking() < JugadorSupToInf.getRanking() ) {
						JugadorSupToInf = jugador;
					}
				}
				
				jugadoresInferiores.add(JugadorSupToInf);
				jugadoresSuperiores.remove(JugadorSupToInf);
			}
    	}
    	
    	Long prueba = 0L;
    	prueba = prueba +1;

    	
    	int mitadSuperior =  (Math.round(jugadoresSuperiores.size()/2));
    	int mitadInferior =  Math.round(jugadoresInferiores.size()/2);

    	
    	for (Jugador jugador : jugadoresSuperiores) {
			if (jugadoresPrimerCuartoSuperior.size()<=mitadSuperior) {
				
				jugadoresPrimerCuartoSuperior.add(jugador);
				jugadoresSuperiores.remove(jugador);
				
			}else {
				jugadoresSegundoCuartoSuperior.add(jugador);
				jugadoresSuperiores.remove(jugador);
			}
		}
    	for (Jugador jugador : jugadoresInferiores) {
			if (jugadoresPrimerCuartoInferior.size()<= mitadInferior) {
				
				jugadoresPrimerCuartoInferior.add(jugador);
				jugadoresInferiores.remove(jugador);
				
			}else {
				jugadoresSegundoCuartoInferior.add(jugador);
				jugadoresInferiores.remove(jugador);
			}
		}

    	return "";
    }
    
    
    
	public List<Jugador> getLosJugadores()  {

		return losJugadores;
	}

	public void setLosJugadores(List<Jugador> losJugadores) {
		this.losJugadores = losJugadores;
	}

	public IBusinessDelegatorView getBusinessDelegatorView() {
		return businessDelegatorView;
	}

	public void setBusinessDelegatorView(IBusinessDelegatorView businessDelegatorView) {
		this.businessDelegatorView = businessDelegatorView;
	}

	public static Logger getLog() {
		return log;
	}


	
}
