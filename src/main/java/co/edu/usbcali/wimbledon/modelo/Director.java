package co.edu.usbcali.wimbledon.modelo;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "director", schema = "public")
public class Director implements java.io.Serializable {
    @NotNull
    private Long dirId;
    private String apellido;
    private Long cedula;
    private String correo;
    private String nombre;
    private Set<Ronda> rondas = new HashSet<Ronda>(0);

    public Director() {
    }

    public Director(Long dirId, String apellido, Long cedula, String correo,
        String nombre, Set<Ronda> rondas) {
        this.dirId = dirId;
        this.apellido = apellido;
        this.cedula = cedula;
        this.correo = correo;
        this.nombre = nombre;
        this.rondas = rondas;
    }

    @Id
    @Column(name = "dir_id", unique = true, nullable = false)
    public Long getDirId() {
        return this.dirId;
    }

    public void setDirId(Long dirId) {
        this.dirId = dirId;
    }

    @Column(name = "apellido")
    public String getApellido() {
        return this.apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Column(name = "cedula")
    public Long getCedula() {
        return this.cedula;
    }

    public void setCedula(Long cedula) {
        this.cedula = cedula;
    }

    @Column(name = "correo")
    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Column(name = "nombre")
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "director")
    public Set<Ronda> getRondas() {
        return this.rondas;
    }

    public void setRondas(Set<Ronda> rondas) {
        this.rondas = rondas;
    }
}
