package co.edu.usbcali.wimbledon.presentation.backingBeans;

import co.edu.usbcali.wimbledon.exceptions.*;
import co.edu.usbcali.wimbledon.modelo.*;
import co.edu.usbcali.wimbledon.modelo.dto.PartidoJugadorDTO;
import co.edu.usbcali.wimbledon.presentation.businessDelegate.*;
import co.edu.usbcali.wimbledon.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class PartidoJugadorView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(PartidoJugadorView.class);
    private InputText txtPuntaje;
    private InputText txtJugId_Jugador;
    private InputText txtParId_Partido;
    private InputText txtParjugId;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<PartidoJugadorDTO> data;
    private PartidoJugadorDTO selectedPartidoJugador;
    private PartidoJugador entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public PartidoJugadorView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedPartidoJugador = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedPartidoJugador = null;

        if (txtPuntaje != null) {
            txtPuntaje.setValue(null);
            txtPuntaje.setDisabled(true);
        }

        if (txtJugId_Jugador != null) {
            txtJugId_Jugador.setValue(null);
            txtJugId_Jugador.setDisabled(true);
        }

        if (txtParId_Partido != null) {
            txtParId_Partido.setValue(null);
            txtParId_Partido.setDisabled(true);
        }

        if (txtParjugId != null) {
            txtParjugId.setValue(null);
            txtParjugId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtId() {
        try {
            Long parjugId = FacesUtils.checkLong(txtParjugId);
            entity = (parjugId != null)
                ? businessDelegatorView.getPartidoJugador(parjugId) : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtPuntaje.setDisabled(false);
            txtJugId_Jugador.setDisabled(false);
            txtParId_Partido.setDisabled(false);
            txtParjugId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtPuntaje.setValue(entity.getPuntaje());
            txtPuntaje.setDisabled(false);
            txtJugId_Jugador.setValue(entity.getJugador().getJugId());
            txtJugId_Jugador.setDisabled(false);
            txtParId_Partido.setValue(entity.getPartido().getParId());
            txtParId_Partido.setDisabled(false);
            txtParjugId.setValue(entity.getParjugId());
            txtParjugId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedPartidoJugador = (PartidoJugadorDTO) (evt.getComponent()
                                                         .getAttributes()
                                                         .get("selectedPartidoJugador"));
        txtPuntaje.setValue(selectedPartidoJugador.getPuntaje());
        txtPuntaje.setDisabled(false);
        txtJugId_Jugador.setValue(selectedPartidoJugador.getJugId_Jugador());
        txtJugId_Jugador.setDisabled(false);
        txtParId_Partido.setValue(selectedPartidoJugador.getParId_Partido());
        txtParId_Partido.setDisabled(false);
        txtParjugId.setValue(selectedPartidoJugador.getParjugId());
        txtParjugId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedPartidoJugador == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new PartidoJugador();

            Long parjugId = FacesUtils.checkLong(txtParjugId);

            entity.setParjugId(parjugId);
            entity.setPuntaje(FacesUtils.checkLong(txtPuntaje));
            entity.setJugador((FacesUtils.checkLong(txtJugId_Jugador) != null)
                ? businessDelegatorView.getJugador(FacesUtils.checkLong(
                        txtJugId_Jugador)) : null);
            entity.setPartido((FacesUtils.checkLong(txtParId_Partido) != null)
                ? businessDelegatorView.getPartido(FacesUtils.checkLong(
                        txtParId_Partido)) : null);
            businessDelegatorView.savePartidoJugador(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long parjugId = new Long(selectedPartidoJugador.getParjugId());
                entity = businessDelegatorView.getPartidoJugador(parjugId);
            }

            entity.setPuntaje(FacesUtils.checkLong(txtPuntaje));
            entity.setJugador((FacesUtils.checkLong(txtJugId_Jugador) != null)
                ? businessDelegatorView.getJugador(FacesUtils.checkLong(
                        txtJugId_Jugador)) : null);
            entity.setPartido((FacesUtils.checkLong(txtParId_Partido) != null)
                ? businessDelegatorView.getPartido(FacesUtils.checkLong(
                        txtParId_Partido)) : null);
            businessDelegatorView.updatePartidoJugador(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedPartidoJugador = (PartidoJugadorDTO) (evt.getComponent()
                                                             .getAttributes()
                                                             .get("selectedPartidoJugador"));

            Long parjugId = new Long(selectedPartidoJugador.getParjugId());
            entity = businessDelegatorView.getPartidoJugador(parjugId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long parjugId = FacesUtils.checkLong(txtParjugId);
            entity = businessDelegatorView.getPartidoJugador(parjugId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deletePartidoJugador(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(Long parjugId, Long puntaje,
        Long jugId_Jugador, Long parId_Partido) throws Exception {
        try {
            entity.setPuntaje(FacesUtils.checkLong(puntaje));
            businessDelegatorView.updatePartidoJugador(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("PartidoJugadorView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtPuntaje() {
        return txtPuntaje;
    }

    public void setTxtPuntaje(InputText txtPuntaje) {
        this.txtPuntaje = txtPuntaje;
    }

    public InputText getTxtJugId_Jugador() {
        return txtJugId_Jugador;
    }

    public void setTxtJugId_Jugador(InputText txtJugId_Jugador) {
        this.txtJugId_Jugador = txtJugId_Jugador;
    }

    public InputText getTxtParId_Partido() {
        return txtParId_Partido;
    }

    public void setTxtParId_Partido(InputText txtParId_Partido) {
        this.txtParId_Partido = txtParId_Partido;
    }

    public InputText getTxtParjugId() {
        return txtParjugId;
    }

    public void setTxtParjugId(InputText txtParjugId) {
        this.txtParjugId = txtParjugId;
    }

    public List<PartidoJugadorDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataPartidoJugador();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<PartidoJugadorDTO> partidoJugadorDTO) {
        this.data = partidoJugadorDTO;
    }

    public PartidoJugadorDTO getSelectedPartidoJugador() {
        return selectedPartidoJugador;
    }

    public void setSelectedPartidoJugador(PartidoJugadorDTO partidoJugador) {
        this.selectedPartidoJugador = partidoJugador;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}
